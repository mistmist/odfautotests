<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns="http://www.example.org/documenttests" xmlns:anim="urn:oasis:names:tc:opendocument:xmlns:animation:1.0" xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0" xmlns:config="urn:oasis:names:tc:opendocument:xmlns:config:1.0" xmlns:db="urn:oasis:names:tc:opendocument:xmlns:database:1.0" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0" xmlns:manifest="urn:oasis:names:tc:opendocument:xmlns:manifest:1.0" xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0" xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datas:1.0" xmlns:o="urn:oasis:names:tc:opendocument:xmlns:office:1.0" xmlns:odf="http://docs.oasis-open.org/ns/office/1.2/meta/odf#" xmlns:of="urn:oasis:names:tc:opendocument:xmlns:of:1.2" xmlns:presentation="urn:oasis:names:tc:opendocument:xmlns:presentation:1.0" xmlns:s="urn:oasis:names:tc:opendocument:xmlns:style:1.0" xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0" xmlns:smil="urn:oasis:names:tc:opendocument:xmlns:smil-compatible:1.0" xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0" xmlns:t="http://www.example.org/documenttests" xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:import href="shared.xsl"/>
  <xsl:param name="mode" select="'odt'"/>
  <xsl:param name="test" select="'graphic'"/>
  <xsl:param name="props" select="'paragraph'"/>
  <t:testtemplates>
    <t:testtemplate name="dr3d-ambient-color">
      <t:test-style>
        <dr3d:ambient-color value="#339999"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-backface-culling-enabled">
      <t:test-style>
        <dr3d:backface-culling value="enabled"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-backface-culling-disabled">
      <t:test-style>
        <dr3d:backface-culling value="disabled"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-back-scale-50p">
      <t:test-style>
        <dr3d:back-scale value="50%"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-close-back-false">
      <t:test-style>
        <dr3d:close-back value="false"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-close-back-true">
      <t:test-style>
        <dr3d:close-back value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-close-front-false">
      <t:test-style>
        <dr3d:close-front value="false"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-close-front-true">
      <t:test-style>
        <dr3d:close-front value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-depth-in">
      <t:test-style>
        <dr3d:depth value="1in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-diffuse-color">
      <t:test-style>
        <dr3d:diffuse-color value="#ff0000"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-edge-rounding-5p">
      <t:test-style>
        <dr3d:edge-rounding value="5%"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-edge-rounding-mode-correct">
      <t:test-style>
        <dr3d:edge-rounding-mode value="correct"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-edge-rounding-mode-attractive">
      <t:test-style>
        <dr3d:edge-rounding-mode value="attractive"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-emissive-color">
      <t:test-style>
        <dr3d:emissive-color value="#ff0000"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-end-angle-deg">
      <t:test-style>
        <dr3d:end-angle value="180deg"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-end-angle-rad">
      <t:test-style>
        <dr3d:end-angle value="3.14rad"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-horizontal-segments-4">
      <t:test-style>
        <dr3d:horizontal-segments value="4"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-horizontal-segments-128">
      <t:test-style>
        <dr3d:horizontal-segments value="128"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-horizontal-segments-256">
      <t:test-style>
        <dr3d:horizontal-segments value="256"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-lighting-mode-standard">
      <t:test-style>
        <dr3d:lighting-mode value="standard"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-lighting-mode-double-sided">
      <t:test-style>
        <dr3d:lighting-mode value="double-sided"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-normals-direction-normal">
      <t:test-style>
        <dr3d:normals-direction value="normal"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-normals-direction-inverse">
      <t:test-style>
        <dr3d:normals-direction value="inverse"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-normals-kind-object">
      <t:test-style>
        <dr3d:normals-kind value="object"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-normals-kind-flat">
      <t:test-style>
        <dr3d:normals-kind value="flat"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-normals-kind-sphere">
      <t:test-style>
        <dr3d:normals-kind value="sphere"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-shadow-visible">
      <t:test-style>
        <dr3d:shadow value="visible"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-shadow-hidden">
      <t:test-style>
        <dr3d:shadow value="hidden"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-shininess-20p">
      <t:test-style>
        <dr3d:shininess value="20%"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-shininess-80p">
      <t:test-style>
        <dr3d:shininess value="80%"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-specular-color">
      <t:test-style>
        <dr3d:specular-color value="#ff0000"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-texture-filter-enabled">
      <t:test-style>
        <dr3d:texture-filter value="enabled"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-texture-filter-disabled">
      <t:test-style>
        <dr3d:texture-filter value="disabled"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-texture-generation-mode-x-object">
      <t:test-style>
        <dr3d:texture-generation-mode-x value="object"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-texture-generation-mode-x-parallel">
      <t:test-style>
        <dr3d:texture-generation-mode-x value="parallel"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-texture-generation-mode-x-sphere">
      <t:test-style>
        <dr3d:texture-generation-mode-x value="sphere"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-texture-generation-mode-y-object">
      <t:test-style>
        <dr3d:texture-generation-mode-y value="object"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-texture-generation-mode-y-parallel">
      <t:test-style>
        <dr3d:texture-generation-mode-y value="parallel"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-texture-generation-mode-y-sphere">
      <t:test-style>
        <dr3d:texture-generation-mode-y value="sphere"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-texture-kind-luminance">
      <t:test-style>
        <dr3d:texture-kind value="luminance"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-texture-kind-intensity">
      <t:test-style>
        <dr3d:texture-kind value="intensity"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-texture-kind-color">
      <t:test-style>
        <dr3d:texture-kind value="color"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-texture-mode-replace">
      <t:test-style>
        <dr3d:texture-mode value="replace"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-texture-mode-modulate">
      <t:test-style>
        <dr3d:texture-mode value="modulate"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-texture-mode-blend">
      <t:test-style>
        <dr3d:texture-mode value="blend"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-vertical-segments-4">
      <t:test-style>
        <dr3d:vertical-segments value="4"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-vertical-segments-128">
      <t:test-style>
        <dr3d:vertical-segments value="128"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="dr3d-vertical-segments-256">
      <t:test-style>
        <dr3d:vertical-segments value="256"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-auto-grow-height-true">
      <t:middle-style>
        <draw:auto-grow-height value="false"/>
      </t:middle-style>
      <t:test-style>
        <draw:auto-grow-height value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-auto-grow-height-false">
      <t:middle-style>
        <draw:auto-grow-height value="true"/>
      </t:middle-style>
      <t:test-style>
        <draw:auto-grow-height value="false"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-auto-grow-width-true">
      <t:middle-style>
        <draw:auto-grow-width value="false"/>
      </t:middle-style>
      <t:test-style>
        <draw:auto-grow-width value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-auto-grow-width-false">
      <t:middle-style>
        <draw:auto-grow-width value="true"/>
      </t:middle-style>
      <t:test-style>
        <draw:auto-grow-width value="false"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-blue-green-red">
      <t:test-style>
        <draw:blue value="50%"/>
        <draw:green value="4%"/>
        <draw:red value="-20%"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-caption-angle-deg">
      <t:test-style>
        <draw:caption-angle value="10deg"/>
        <draw:caption-angle-type value="fixed"/>
      </t:test-style>
      <t:test-bodytext pagecaption="1"/>
    </t:testtemplate>
    <t:testtemplate name="draw-caption-angle-rad">
      <t:test-style>
        <draw:caption-angle value="1rad"/>
        <draw:caption-angle-type value="fixed"/>
      </t:test-style>
      <t:test-bodytext pagecaption="1"/>
    </t:testtemplate>
    <t:testtemplate name="draw-caption-angle-type-fixed">
      <t:test-style>
        <draw:caption-angle-type value="fixed"/>
      </t:test-style>
      <t:test-bodytext pagecaption="1"/>
    </t:testtemplate>
    <t:testtemplate name="draw-caption-angle-type-free">
      <t:test-style>
        <draw:caption-angle-type value="free"/>
      </t:test-style>
      <t:test-bodytext pagecaption="1"/>
    </t:testtemplate>
    <t:testtemplate name="draw-caption-escape-in">
      <t:test-style>
        <draw:caption-escape value="1in"/>
      </t:test-style>
      <t:test-bodytext pagecaption="1"/>
    </t:testtemplate>
    <t:testtemplate name="draw-caption-escape-cm">
      <t:test-style>
        <draw:caption-escape value="3cm"/>
      </t:test-style>
      <t:test-bodytext pagecaption="1"/>
    </t:testtemplate>
    <t:testtemplate name="draw-caption-escape-p">
      <t:test-style>
        <draw:caption-escape value="10%"/>
      </t:test-style>
      <t:test-bodytext pagecaption="1"/>
    </t:testtemplate>
    <t:testtemplate name="draw-caption-escape-direction-horizontal">
      <t:test-style>
        <draw:caption-escape-direction value="horizontal"/>
      </t:test-style>
      <t:test-bodytext pagecaption="1"/>
    </t:testtemplate>
    <t:testtemplate name="draw-caption-escape-direction-vertical">
      <t:test-style>
        <draw:caption-escape-direction value="vertical"/>
      </t:test-style>
      <t:test-bodytext pagecaption="1"/>
    </t:testtemplate>
    <t:testtemplate name="draw-caption-escape-direction-auto">
      <t:test-style>
        <draw:caption-escape-direction value="auto"/>
      </t:test-style>
      <t:test-bodytext pagecaption="1"/>
    </t:testtemplate>
    <t:testtemplate name="draw-caption-fit-line-length-false">
      <t:middle-style>
        <draw:caption-fit-line-length value="true"/>
      </t:middle-style>
      <t:test-style>
        <draw:caption-fit-line-length value="false"/>
      </t:test-style>
      <t:test-bodytext pagecaption="1"/>
    </t:testtemplate>
    <t:testtemplate name="draw-caption-fit-line-length-true">
      <t:middle-style>
        <draw:caption-fit-line-length value="false"/>
      </t:middle-style>
      <t:test-style>
        <draw:caption-fit-line-length value="true"/>
      </t:test-style>
      <t:test-bodytext pagecaption="1"/>
    </t:testtemplate>
    <t:testtemplate name="draw-caption-gap-in">
      <t:test-style>
        <draw:caption-gap value="1in"/>
      </t:test-style>
      <t:test-bodytext pagecaption="1"/>
    </t:testtemplate>
    <t:testtemplate name="draw-caption-gap-cm">
      <t:test-style>
        <draw:caption-gap value="3cm"/>
      </t:test-style>
      <t:test-bodytext pagecaption="1"/>
    </t:testtemplate>
    <t:testtemplate name="draw-caption-line-length-cm">
      <t:test-style>
        <draw:caption-line-length value="30cm"/>
        <draw:caption-fit-line-length value="false"/>
      </t:test-style>
      <t:test-bodytext pagecaption="1"/>
    </t:testtemplate>
    <t:testtemplate name="draw-caption-line-length-in">
      <t:test-style>
        <draw:caption-line-length value="10in"/>
        <draw:caption-fit-line-length value="false"/>
      </t:test-style>
      <t:test-bodytext pagecaption="1"/>
    </t:testtemplate>
    <t:testtemplate name="draw-caption-type-straight-line">
      <t:test-style>
        <draw:caption-type value="straight-line"/>
      </t:test-style>
      <t:test-bodytext pagecaption="1"/>
    </t:testtemplate>
    <t:testtemplate name="draw-caption-type-angled-line">
      <t:test-style>
        <draw:caption-type value="angled-line"/>
      </t:test-style>
      <t:test-bodytext pagecaption="1"/>
    </t:testtemplate>
    <t:testtemplate name="draw-caption-type-angled-connector-line">
      <t:test-style>
        <draw:caption-type value="angled-connector-line"/>
      </t:test-style>
      <t:test-bodytext pagecaption="1"/>
    </t:testtemplate>
    <t:testtemplate name="draw-color-inversion-true">
      <t:test-style>
        <draw:color-inversion value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-color-mode-greyscale">
      <t:test-style>
        <draw:color-mode value="greyscale"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-color-mode-mono">
      <t:test-style>
        <draw:color-mode value="mono"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-color-mode-watermark">
      <t:test-style>
        <draw:color-mode value="watermark"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-color-mode-standard">
      <t:middle-style>
        <draw:color-mode value="mono"/>
      </t:middle-style>
      <t:test-style>
        <draw:color-mode value="standard"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-contrast-50p">
      <t:test-style>
        <draw:contrast value="50%"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-decimal-places-5">
      <t:test-style>
        <draw:decimal-places value="5"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-aspect-content">
      <t:test-style>
        <draw:draw-aspect value="content"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-aspect-thumbnail">
      <t:test-style>
        <draw:draw-aspect value="thumbnail"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-aspect-icon">
      <t:test-style>
        <draw:draw-aspect value="icon"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-aspect-print-view">
      <t:test-style>
        <draw:draw-aspect value="print-view"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-end-guide-in">
      <t:test-style>
        <draw:end-guide value="1in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-end-guide-cm">
      <t:test-style>
        <draw:end-guide value="3cm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="end-line-spacing-horizontal-in">
      <t:test-style>
        <draw:end-line-spacing-horizontal value="1in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="end-line-spacing-horizontal-cm">
      <t:test-style>
        <draw:end-line-spacing-horizontal value="3cm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="end-line-spacing-vertical-in">
      <t:test-style>
        <draw:end-line-spacing-vertical value="1in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="end-line-spacing-vertical-cm">
      <t:test-style>
        <draw:end-line-spacing-vertical value="3cm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fill-none">
      <t:test-style>
        <draw:fill value="none"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fill-solid">
      <t:test-style>
        <draw:fill value="solid"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fill-bitmap">
      <t:test-style>
        <draw:fill value="bitmap"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fill-gradient">
      <t:test-style>
        <draw:fill value="gradient"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fill-hatch">
      <t:test-style>
        <draw:fill value="hatch"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fill-color">
      <t:test-style>
        <draw:fill-color value="#ff0000"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="fraw-fill-gradient-name">
      <t:test-style>
        <draw:fill-gradient-name value="gradStyle"/>
        <draw:fill value="gradient"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="fraw-fill-hatch-name">
      <t:test-style>
        <draw:fill-hatch-name value="hatchStyle"/>
        <draw:fill value="hatch"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="fraw-fill-hatch-solid-false">
      <t:test-style>
        <draw:fill-hatch-solid value="false"/>
        <draw:fill-hatch-name value="hatchStyle"/>
        <draw:fill value="hatch"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="fraw-fill-hatch-solid-true">
      <t:test-style>
        <draw:fill-hatch-solid value="true"/>
        <draw:fill-hatch-name value="hatchStyle"/>
        <draw:fill value="hatch"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fill-image-height-in">
      <t:test-style>
        <draw:fill-image-height value="1in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fill-image-height-cm">
      <t:test-style>
        <draw:fill-image-height value="3cm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fill-image-height-p">
      <t:test-style>
        <draw:fill-image-height value="50%"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fill-image-name">
      <t:test-style>
        <draw:fill-image-name value="imageStyle"/>
        <draw:fill value="bitmap"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fill-image-ref-point-top-left">
      <t:test-style>
        <draw:fill-image-ref-point value="top-left"/>
        <s:repeat value="repeat"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fill-image-ref-point-top">
      <t:test-style>
        <draw:fill-image-ref-point value="top"/>
        <s:repeat value="repeat"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fill-image-ref-point-top-right">
      <t:test-style>
        <draw:fill-image-ref-point value="top-right"/>
        <s:repeat value="repeat"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fill-image-ref-point-left">
      <t:test-style>
        <draw:fill-image-ref-point value="left"/>
        <s:repeat value="repeat"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fill-image-ref-point-center">
      <t:test-style>
        <draw:fill-image-ref-point value="center"/>
        <s:repeat value="repeat"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fill-image-ref-point-right">
      <t:test-style>
        <draw:fill-image-ref-point value="right"/>
        <s:repeat value="repeat"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fill-image-ref-point-bottom-left">
      <t:test-style>
        <draw:fill-image-ref-point value="bottom-left"/>
        <s:repeat value="repeat"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fill-image-ref-point-bottom">
      <t:test-style>
        <draw:fill-image-ref-point value="bottom"/>
        <s:repeat value="repeat"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fill-image-ref-point-x-50p">
      <t:test-style>
        <draw:fill-image-ref-point-x value="50%"/>
        <s:repeat value="repeat"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fill-image-width-in">
      <t:test-style>
        <draw:fill-image-width value="5in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fill-image-width-cm">
      <t:test-style>
        <draw:fill-image-width value="15cm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fill-image-width-50p">
      <t:test-style>
        <draw:fill-image-width value="50%"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fit-to-contour-true">
      <t:test-style>
        <draw:fit-to-contour value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fit-to-contour-false">
      <t:test-style>
        <draw:fit-to-contour value="false"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fit-to-size-true">
      <t:test-style>
        <draw:fit-to-size value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-fit-to-size-false">
      <t:middle-style>
        <draw:fit-to-size value="true"/>
      </t:middle-style>
      <t:test-style>
        <draw:fit-to-size value="false"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-frame-display-border-true">
      <t:test-style>
        <draw:frame-display-border value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-frame-display-border-false">
      <t:middle-style>
        <draw:frame-display-border value="true"/>
      </t:middle-style>
      <t:test-style>
        <draw:frame-display-border value="false"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-frame-display-scrollbar-true">
      <t:test-style>
        <draw:frame-display-scrollbar value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-frame-display-scrollbar-false">
      <t:middle-style>
        <draw:frame-display-scrollbar value="true"/>
      </t:middle-style>
      <t:test-style>
        <draw:frame-display-scrollbar value="false"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-frame-margin-horizontal-10px">
      <t:test-style>
        <draw:frame-margin-horizontal value="10px"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-frame-margin-vertical-10px">
      <t:test-style>
        <draw:frame-margin-vertical value="10px"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-gamma-50p">
      <t:test-style>
        <draw:gamma value="50%"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-gradient-step-count-4">
      <t:test-style>
        <draw:gradient-step-count value="4"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-gradient-step-count-128">
      <t:test-style>
        <draw:gradient-step-count value="128"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-gradient-step-count-512">
      <t:test-style>
        <draw:gradient-step-count value="512"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-guide-distance">
      <t:test-style>
        <draw:guide-distance value="10px"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-guide-overhang-in">
      <t:test-style>
        <draw:guide-overhang value="1in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-guide-overhang-cm">
      <t:test-style>
        <draw:guide-overhang value="3cm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-image-opacity">
      <t:test-style>
        <draw:image-opacity value="50%"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-line-distance">
      <t:test-style>
        <draw:line-distance value="10px"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-luminance-50p">
      <t:test-style>
        <draw:luminance value="50%"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-marker-end">
      <t:test-style>
        <draw:marker-end value="endStyle"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-marker-end-center-true">
      <t:test-style>
        <draw:marker-end-center value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-marker-end-center-false">
      <t:middle-style>
        <draw:marker-end-center value="true"/>
      </t:middle-style>
      <t:test-style>
        <draw:marker-end-center value="false"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-marker-end-width-in">
      <t:test-style>
        <draw:marker-end-width value="1in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-marker-end-width-cm">
      <t:test-style>
        <draw:marker-end-width value="3cm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-marker-start">
      <t:test-style>
        <draw:marker-start value="startStyle"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-marker-start-center-true">
      <t:test-style>
        <draw:marker-start-center value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-marker-start-center-false">
      <t:middle-style>
        <draw:marker-start-center value="true"/>
      </t:middle-style>
      <t:test-style>
        <draw:marker-start-center value="false"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-marker-start-width-in">
      <t:test-style>
        <draw:marker-start-width value="1in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-marker-start-width-cm">
      <t:test-style>
        <draw:marker-start-width value="3cm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-measure-align-automatic">
      <t:middle-style>
        <draw:measure-align value="inside"/>
      </t:middle-style>
      <t:test-style>
        <draw:measure-align value="automatic"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-measure-align-left-outside">
      <t:test-style>
        <draw:measure-align value="left-outside"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-measure-align-inside">
      <t:test-style>
        <draw:measure-align value="inside"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-measure-align-right-outside">
      <t:test-style>
        <draw:measure-align value="right-outside"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-measure-vertical-align-automatic">
      <t:middle-style>
        <draw:measure-vertical-align value="above"/>
      </t:middle-style>
      <t:test-style>
        <draw:measure-vertical-align value="automatic"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-measure-vertical-align-above">
      <t:test-style>
        <draw:measure-vertical-align value="above"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-measure-vertical-align-below">
      <t:test-style>
        <draw:measure-vertical-align value="below"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-measure-vertical-align-center">
      <t:test-style>
        <draw:measure-vertical-align value="center"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-opacity-50p">
      <t:test-style>
        <draw:opacity value="50%"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-opacity-name">
      <t:test-style>
        <draw:opacity-name value="opacityStyle"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-parallel-false">
      <t:test-style>
        <draw:parallel value="false"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-parallel-true">
      <t:middle-style>
        <draw:parallel value="false"/>
      </t:middle-style>
      <t:test-style>
        <draw:parallel value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-placing-above">
      <t:middle-style>
        <draw:placing value="below"/>
      </t:middle-style>
      <t:test-style>
        <draw:placing value="above"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-placing-below">
      <t:middle-style>
        <draw:placing value="above"/>
      </t:middle-style>
      <t:test-style>
        <draw:placing value="below"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-secondary-fill-color">
      <t:test-style>
        <draw:secondary-fill-color value="#ff0000"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-shadow-visible">
      <t:test-style>
        <draw:shadow value="visible"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-shadow-color">
      <t:test-style>
        <draw:shadow value="visible"/>
        <draw:shadow-color value="#ff0000"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-shadow-offset-x">
      <t:test-style>
        <draw:shadow value="visible"/>
        <draw:shadow-color value="#ff0000"/>
        <draw:shadow-offset-x value="3px"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-shadow-offset-y">
      <t:test-style>
        <draw:shadow value="visible"/>
        <draw:shadow-color value="#ff0000"/>
        <draw:shadow-offset-y value="3px"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-show-unit">
      <t:test-style>
        <draw:show-unit value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-start-guide-in">
      <t:test-style>
        <draw:start-guide value="1in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-start-guide-cm">
      <t:test-style>
        <draw:start-guide value="3cm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-start-line-spacing-horizontal">
      <t:test-style>
        <draw:start-line-spacing-horizontal value="10px"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-start-line-spacing-vertical">
      <t:test-style>
        <draw:start-line-spacing-vertical value="10px"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-stroke-dash">
      <t:test-style>
        <draw:stroke value="dash"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-stroke-solid">
      <t:test-style>
        <draw:stroke value="solid"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-stroke-dashstyle">
      <t:test-style>
        <draw:stroke-dash value="dashStyle"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-stroke-dash-names">
      <t:test-style>
        <draw:stroke-dash-names value="dashStyle"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-stroke-linejoin-miter">
      <t:test-style>
        <draw:stroke-linejoin value="miter"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-stroke-linejoin-round">
      <t:test-style>
        <draw:stroke-linejoin value="round"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-stroke-linejoin-bevel">
      <t:test-style>
        <draw:stroke-linejoin value="bevel"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-stroke-linejoin-middle">
      <t:test-style>
        <draw:stroke-linejoin value="middle"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-symbol-color">
      <t:test-style>
        <draw:symbol-color value="#ff0000"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-textarea-horizontal-align-left">
      <t:test-style>
        <draw:textarea-horizontal-align value="left"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-textarea-horizontal-align-center">
      <t:test-style>
        <draw:textarea-horizontal-align value="center"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-textarea-horizontal-align-right">
      <t:test-style>
        <draw:textarea-horizontal-align value="right"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-textarea-horizontal-align-justify">
      <t:test-style>
        <draw:textarea-horizontal-align value="justify"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-textarea-vertical-align-top">
      <t:test-style>
        <draw:textarea-vertical-align value="top"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-textarea-vertical-align-middle">
      <t:test-style>
        <draw:textarea-vertical-align value="middle"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-textarea-vertical-align-bottom">
      <t:test-style>
        <draw:textarea-vertical-align value="bottom"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-textarea-vertical-align-justify">
      <t:test-style>
        <draw:textarea-vertical-align value="justify"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-tile-repeat-offset">
      <t:test-style>
        <draw:tile-repeat-offset value="50% horizontal"/>
        <s:repeat value="repeat"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-unit-mm">
      <t:test-style>
        <draw:unit value="mm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-unit-cm">
      <t:test-style>
        <draw:unit value="cm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-unit-m">
      <t:test-style>
        <draw:unit value="m"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-unit-km">
      <t:test-style>
        <draw:unit value="km"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-unit-pt">
      <t:test-style>
        <draw:unit value="pt"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-unit-pc">
      <t:test-style>
        <draw:unit value="pc"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-unit-in">
      <t:test-style>
        <draw:unit value="inch"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-unit-ft">
      <t:test-style>
        <draw:unit value="ft"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-unit-mi">
      <t:test-style>
        <draw:unit value="mi"/>
      </t:test-style>
    </t:testtemplate>
    <!--
	      skip draw:visible-area-height 20.169,
	      skip draw:visible-area-left 20.170,
	      skip draw:visible-area-top 20.171,
	      skip draw:visible-area-width 20.172,
	  -->
    <t:testtemplate name="draw-wrap-influence-on-position-iterative">
      <t:test-style>
        <draw:wrap-influence-on-position value="iterative"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-wrap-influence-on-position-once-concurrent">
      <t:test-style>
        <draw:wrap-influence-on-position value="once-concurrent"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="draw-wrap-influence-on-position-once-successive">
      <t:test-style>
        <draw:wrap-influence-on-position value="once-successive"/>
      </t:test-style>
    </t:testtemplate>
    <!-- ////////////////// -->
    <!-- ////////////////// -->
    <!-- ////////////////// -->
    <t:testtemplate name="background-color">
      <t:test-style>
        <fo:background-color value="#339999"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border">
      <t:test-style>
        <fo:border value="2pt solid #000000"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border-bottom">
      <t:test-style>
        <fo:border-bottom value="2pt solid #000000"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border-left">
      <t:test-style>
        <fo:border-left value="2pt solid #000000"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border-right">
      <t:test-style>
        <fo:border-right value="2pt solid #000000"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border-top">
      <t:test-style>
        <fo:border-top value="2pt solid #000000"/>
      </t:test-style>
    </t:testtemplate>
    <!-- fo:clip 20.179, -->
    <t:testtemplate name="margin">
      <t:test-style>
        <fo:margin value="2in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="margin-bottom">
      <t:test-style>
        <fo:margin-bottom value="1in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="margin-left">
      <t:test-style>
        <fo:margin-left value="1in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="margin-right">
      <t:test-style>
        <fo:margin-right value="1in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="margin-top">
      <t:test-style>
        <fo:margin-top value="1in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="fo-max-height-in">
      <t:test-style>
        <fo:max-height value="1in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="fo-max-height-cm">
      <t:test-style>
        <fo:max-height value="3cm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="fo-max-height-50p">
      <t:test-style>
        <fo:max-height value="50%"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="fo-max-width-in">
      <t:test-style>
        <fo:max-width value="1in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="fo-max-width-cm">
      <t:test-style>
        <fo:max-width value="3cm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="fo-max-width-50p">
      <t:test-style>
        <fo:max-width value="50%"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="fo-min-height-in">
      <t:test-style>
        <fo:min-height value="1in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="fo-min-height-cm">
      <t:test-style>
        <fo:min-height value="3cm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="fo-min-height-50p">
      <t:test-style>
        <fo:min-height value="50%"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="fo-min-width-in">
      <t:test-style>
        <fo:min-width value="1in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="fo-min-width-cm">
      <t:test-style>
        <fo:min-width value="3cm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="fo-min-width-50p">
      <t:test-style>
        <fo:min-width value="50%"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="padding">
      <t:test-style>
        <fo:padding value="2in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="padding-bottom">
      <t:test-style>
        <fo:padding-bottom value="1.5in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="padding-left">
      <t:test-style>
        <fo:padding-left value="1.5in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="padding-right">
      <t:test-style>
        <fo:padding-right value="1.5in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="padding-top">
      <t:test-style>
        <fo:padding-top value="1.5in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="wrap-option">
      <t:middle-style>
        <fo:wrap-option value="wrap"/>
      </t:middle-style>
      <t:test-style>
        <fo:wrap-option value="no-wrap"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="background-transparency">
      <t:test-style>
        <s:background-transparency value="50%"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border-line-width">
      <t:test-style>
        <fo:border value="5pt double #000000"/>
        <s:border-line-width value="0.4mm 0.0299in 0.7mm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border-line-width-bottom">
      <t:test-style>
        <fo:border-bottom value="5pt double #000000"/>
        <s:border-line-width-bottom value="0.4mm 0.0299in 0.7mm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border-line-width-left">
      <t:test-style>
        <fo:border-left value="5pt double #000000"/>
        <s:border-line-width-left value="0.4mm 0.0299in 0.7mm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border-line-width-right">
      <t:test-style>
        <fo:border-right value="5pt double #000000"/>
        <s:border-line-width-right value="0.4mm 0.0299in 0.7mm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border-line-width-top">
      <t:test-style>
        <fo:border-top value="5pt double #000000"/>
        <s:border-line-width-top value="0.4mm 0.0299in 0.7mm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="editable">
      <t:test-style>
        <s:editable value="false"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="protect">
      <t:test-style>
        <s:editable value="true"/>
      </t:test-style>
    </t:testtemplate>
    <!--  style:flow-with-text 20.259,  -->
    <!--  style:horizontal-pos 20.290,  -->
    <!-- style:horizontal-rel 20.291,  -->
    <t:testtemplate name="style-mirror-nh">
      <t:test-style>
        <s:mirror value="horizontal"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-mirror-vhon">
      <t:test-style>
        <s:mirror value="vertical horizontal-on-even"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-number-wrapped-paragraphs">
      <t:test-style>
        <s:number-wrapped-paragraphs value="2"/>
        <s:wrap value="right"/>
      </t:test-style>
    </t:testtemplate>
    <!-- style:number-wrapped-paragraphs 20.318,  -->
    <t:testtemplate name="style-overflow-behavior-clip">
      <t:test-style>
        <s:overflow-behavior value="clip"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-overflow-behavior-newframe">
      <t:test-style>
        <s:overflow-behavior value="auto-create-new-frame"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-print-content-false">
      <t:test-style>
        <s:print-content value="false"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-print-content-true">
      <t:test-style>
        <s:print-content value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-protect-content">
      <t:test-style>
        <s:protect value="content"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-protect-position">
      <t:test-style>
        <s:protect value="position"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-protect-size">
      <t:test-style>
        <s:protect value="size"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-rel-height-50p">
      <t:test-style>
        <s:rel-height value="50%"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-rel-width-50p">
      <t:test-style>
        <s:rel-width value="50%"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-repeat-no">
      <t:test-style>
        <s:repeat value="no-repeat"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-repeat-repeat">
      <t:test-style>
        <s:repeat value="repeat"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-repeat-stretch">
      <t:test-style>
        <s:repeat value="stretch"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-run-through-foreground">
      <t:test-style>
        <s:run-through value="foreground"/>
        <s:wrap value="run-through"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-run-through-background">
      <t:test-style>
        <s:run-through value="background"/>
        <s:wrap value="run-through"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="shadow">
      <t:test-style>
        <s:shadow value="#808080 -0.0701in 0.0701in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-shrink-to-fit-false">
      <t:middle-style>
        <s:shrink-to-fit value="true"/>
      </t:middle-style>
      <t:test-style>
        <s:shrink-to-fit value="false"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-shrink-to-fit-true">
      <t:middle-style>
        <s:shrink-to-fit value="false"/>
      </t:middle-style>
      <t:test-style>
        <s:shrink-to-fit value="true"/>
      </t:test-style>
    </t:testtemplate>
    <!-- style:vertical-pos 20.387,  -->
    <!-- style:vertical-rel 20.388,  -->
    <t:testtemplate name="style-wrap-none">
      <t:middle-style>
        <s:wrap value="left"/>
      </t:middle-style>
      <t:test-style>
        <s:wrap value="none"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-wrap-left">
      <t:test-style>
        <s:wrap value="left"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-wrap-right">
      <t:test-style>
        <s:wrap value="right"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-wrap-parallel">
      <t:test-style>
        <s:wrap value="parallel"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-wrap-dynamic">
      <t:test-style>
        <s:wrap value="dynamic"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-wrap-run-through">
      <t:test-style>
        <s:wrap value="run-through"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-wrap-biggest">
      <t:test-style>
        <s:wrap value="biggest"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-wrap-contour-false">
      <t:middle-style>
        <s:wrap-contour value="true"/>
      </t:middle-style>
      <t:test-style>
        <s:wrap-contour value="false"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-wrap-contour-true">
      <t:middle-style>
        <s:wrap-contour value="false"/>
      </t:middle-style>
      <t:test-style>
        <s:wrap-contour value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-wrap-contour-mode-full">
      <t:test-style>
        <s:wrap-contour-mode value="full"/>
        <s:wrap value="left"/>
        <s:wrap-contour value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-wrap-contour-mode-outside">
      <t:test-style>
        <s:wrap-contour-mode value="outside"/>
        <s:wrap value="left"/>
        <s:wrap-contour value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-wrap-dynamic-threshold-in">
      <t:test-style>
        <s:wrap-dynamic-threshold value="1in"/>
        <s:wrap value="dynamic"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="style-wrap-dynamic-threshold-cm">
      <t:test-style>
        <s:wrap-dynamic-threshold value="3cm"/>
        <s:wrap value="dynamic"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="writing-mode">
      <t:middle-style>
        <s:writing-mode value="rl"/>
      </t:middle-style>
      <t:test-style>
        <s:writing-mode value="lr-tb"/>
      </t:test-style>
    </t:testtemplate>
    <!--

svg:fill-rule 20.396,
svg:height 20.397.1,
svg:stroke-color 20.398,
svg:stroke-linecap 20.164,
svg:stroke-opacity 20.399,
svg:stroke-width 20.400,
svg:width 20.403,
svg:x 20.401,
svg:y 20.402.1,
text:anchor-page-number 20.407,
text:anchor-type 20.408,
text:animation 20.409,
text:animation-delay 20.410,
text:animation-direction 20.411,
text:animation-repeat 20.412,
text:animation-start-inside 20.413,
text:animation-steps 20.414 and
text:animation-stop-inside 20.415.
-->
  </t:testtemplates>
  <xsl:output encoding="utf-8" indent="no" method="xml" omit-xml-declaration="no"/>
  <xsl:template match="t:testtemplate">
    <xsl:variable name="family">
      <xsl:choose>
        <xsl:when test="$mode='ods'">
          <xsl:value-of select="'table-cell'"/>
        </xsl:when>
        <xsl:when test="$mode='odp'">
          <xsl:value-of select="'graphic'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'paragraph'"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <test name="{$mode}-{@name}">
      <input type="{$mode}1.2">
        <o:styles>
          <s:style s:family="graphic" s:name="middle" s:parent-style-name="Graphics">
            <s:graphic-properties>
              <xsl:for-each select="t:middle-style/*">
                <xsl:attribute name="{name()}" namespace="{namespace-uri()}">
                  <xsl:value-of select="@value"/>
                </xsl:attribute>
              </xsl:for-each>
            </s:graphic-properties>
          </s:style>
          <s:style s:family="graphic" s:name="TestStyle" s:parent-style-name="middle">
            <s:graphic-properties>
              <xsl:for-each select="t:test-style/*">
                <xsl:attribute name="{name()}" namespace="{namespace-uri()}">
                  <xsl:value-of select="@value"/>
                </xsl:attribute>
              </xsl:for-each>
            </s:graphic-properties>
          </s:style>
          <s:style s:family="paragraph" s:name="standard">
            <s:text-properties s:font-name="Helvetica" fo:font-size="12pt"/>
          </s:style>
          <s:style s:family="graphic" s:name="standard">
            <s:graphic-properties draw:fill="none" draw:stroke="none"/>
            <s:text-properties s:font-name="Helvetica" fo:font-size="12pt"/>
          </s:style>
          <s:style s:family="table-cell" s:name="standard">
            <s:text-properties s:font-name="Helvetica" fo:font-size="12pt"/>
          </s:style>
          <s:style s:display-name="Text" s:family="text" s:name="color">
            <s:text-properties fo:color="#339999"/>
          </s:style>
          <s:style s:display-name="paraStyle" s:family="{$family}" s:name="style" s:parent-style-name="standard">
            <s:paragraph-properties/>
          </s:style>
          <s:style s:family="table" s:master-page-name="Standard" s:name="table">
            <s:table-properties s:writing-mode="lr-tb" table:display="true"/>
          </s:style>
        </o:styles>
        <o:automatic-styles>
          <s:style s:family="table" s:master-page-name="Standard" s:name="table">
            <s:table-properties s:writing-mode="lr-tb" table:display="true"/>
          </s:style>
        </o:automatic-styles>
        <xsl:choose>
          <xsl:when test="t:test-bodytext[@pagecaption]">
            <xsl:call-template name="body">
              <xsl:with-param name="pagecaption" select="1"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name="body">
				    </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </input>
      <output types="{$mode}1.0 {$mode}1.1 {$mode}1.2 {$mode}1.2ext">
        <file path="styles.xml">
          <xsl:for-each select="t:test-style/*">
            <xsl:variable name="selector" select="concat(&quot;//s:style[@s:display-name='TestStyle' or (not(@s:display-name) and @s:name='TestStyle')]/s:graphic-properties/@&quot;,name())"/>
            <xpath expr="boolean({$selector})"/>
            <xsl:call-template name="xpaths">
              <xsl:with-param name="selector" select="$selector"/>
              <xsl:with-param name="value" select="@value"/>
              <xsl:with-param name="index" select="-1"/>
            </xsl:call-template>
          </xsl:for-each>
        </file>
      </output>
      <pdf/>
    </test>
  </xsl:template>
  <xsl:template match="/">
    <documenttests xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.example.org/documenttests ../documenttests.xsd">
      <xsl:variable name="tests" select="/xsl:stylesheet/t:testtemplates/*"/>
      <xsl:apply-templates select="$tests"/>
    </documenttests>
  </xsl:template>
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
