<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns="http://www.example.org/documenttests" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:o="urn:oasis:names:tc:opendocument:xmlns:office:1.0" xmlns:s="urn:oasis:names:tc:opendocument:xmlns:style:1.0" xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0" xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0" xmlns:t="http://www.example.org/documenttests" xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:variable name="ipsum">

	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	</xsl:variable>
  <xsl:variable name="Loremipsum"><xsl:value-of select="$ipsum"/>

    	But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?

    	On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammeled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.

    	But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?

    	On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammeled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.

    	But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?

    	On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammeled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.

    	But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?

    	On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammeled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.

        </xsl:variable>
  <xsl:template name="isLength">
    <xsl:param name="value"/>
    <xsl:variable name="len" select="string-length($value)"/>
    <xsl:variable name="suf" select="substring($value,$len - 1,$len)"/>
    <xsl:variable name="num" select="substring($value,1,$len - 2)"/>
    <xsl:variable name="hasLengthUnit" select="$suf = 'cm' or $suf = 'mm' or $suf = 'in' or $suf = 'pt' or $suf = 'pc' or $suf = 'px'"/>
    <xsl:variable name="hasValidNumber" select="string-length(translate($num,'1234567890.-',''))=0"/>
    <xsl:value-of select="$hasLengthUnit and $hasValidNumber"/>
  </xsl:template>
  <xsl:template name="body">
    <xsl:param name="bodytext"/>
    <xsl:param name="threepages"/>
    <xsl:param name="threeshorttext"/>
    <xsl:param name="pageswithstyle"/>
    <xsl:param name="fieldspage"/>
    <xsl:param name="pagefootnote"/>
    <xsl:param name="testpage"/>
    <xsl:param name="pagetable"/>
    <xsl:param name="pagenumberedlist"/>
    <xsl:param name="pageruby"/>
    <xsl:param name="pagecaption"/>
    <xsl:param name="pageoblique"/>
    <xsl:choose>
      <xsl:when test="$test='table'">
        <xsl:choose>
          <xsl:when test="$mode='ods'">
		</xsl:when>
          <xsl:when test="$mode='odp'">
		</xsl:when>
          <xsl:otherwise>
            <o:text>
              <table:table table:name="mytable" table:style-name="mytable">
                <table:table-column table:number-columns-repeated="4" table:style-name="mytableCol"/>
                <table:table-header-rows>
                  <table:table-row>
                    <table:table-cell o:value-type="string" table:style-name="mytableh1">
                      <text:p text:style-name="P2">item</text:p>
                    </table:table-cell>
                    <table:table-cell o:value-type="string" table:style-name="mytableh1">
                      <text:p text:style-name="P2">value1</text:p>
                    </table:table-cell>
                    <table:table-cell o:value-type="string" table:style-name="mytableh1">
                      <text:p text:style-name="P2">value2</text:p>
                    </table:table-cell>
                    <table:table-cell o:value-type="string" table:style-name="mytableh2">
                      <text:p text:style-name="Table_20_Heading"/>
                    </table:table-cell>
                  </table:table-row>
                </table:table-header-rows>
                <table:table-row>
                  <table:table-cell o:value-type="string" table:protected="true" table:style-name="style">
                    <text:p text:style-name="P1">love</text:p>
                  </table:table-cell>
                  <table:table-cell o:value-type="string" table:style-name="style">
                    <text:p text:style-name="P1">5</text:p>
                  </table:table-cell>
                  <table:table-cell o:value-type="string" table:style-name="style">
                    <text:p text:style-name="P1">83</text:p>
                  </table:table-cell>
                  <table:table-cell o:value-type="string" table:style-name="mytablec2">
                    <text:p text:style-name="Table_20_Contents"/>
                  </table:table-cell>
                </table:table-row>
                <table:table-row>
                  <table:table-cell o:value-type="string" table:style-name="style">
                    <text:p text:style-name="P1">ODF</text:p>
                  </table:table-cell>
                  <table:table-cell o:value-type="string" table:style-name="style">
                    <text:p text:style-name="P1">11</text:p>
                  </table:table-cell>
                  <table:table-cell o:value-type="string" table:style-name="style">
                    <text:p text:style-name="P1">37.8</text:p>
                  </table:table-cell>
                  <table:table-cell o:value-type="string" table:style-name="mytablec2">
                    <text:p text:style-name="P1">0</text:p>
                  </table:table-cell>
                </table:table-row>
              </table:table>
            </o:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:when test="$test='section'">
        <xsl:choose>
          <xsl:when test="$mode='ods'">
		</xsl:when>
          <xsl:when test="$mode='odp'">
		</xsl:when>
          <xsl:otherwise>
            <o:text>
              <text:section text:name="mySection" text:style-name="style">
                <text:p>hello world. HELLO WORLD.</text:p>
              </text:section>
            </o:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:when test="$test='graphic'">
        <xsl:choose>
          <xsl:when test="$mode='ods'">
		</xsl:when>
          <xsl:when test="$mode='odp'">
		</xsl:when>
          <xsl:otherwise>
            <o:text>
              <draw:frame draw:name="Frame1" draw:z-index="0" svg:width="17.59cm" text:anchor-type="paragraph">
                <draw:text-box fo:min-height="12.862cm">
                  <text:p text:style-name="Standard"><draw:frame draw:name="Image1" draw:style-name="teststyle" draw:z-index="0" svg:height="2.0cm" svg:width="2.0cm" text:anchor-type="paragraph"><draw:image xlink:actuate="onLoad" xlink:href="Pictures/philips.svg" xlink:show="embed" xlink:type="simple"/></draw:frame>Illustration
		<text:sequence s:num-format="1" text:name="Illustration" text:ref-name="refIllustration0">1</text:sequence>
		: mycaption123
		    </text:p>
                </draw:text-box>
              </draw:frame>
            </o:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:when test="$test='page'">
        <xsl:choose>
          <xsl:when test="$mode='ods'">
		</xsl:when>
          <xsl:when test="$mode='odp'">
		</xsl:when>
          <xsl:otherwise>
            <o:text>
              <xsl:choose>
                <xsl:when test="string-length($pagefootnote)!=0">
                  <text:p>This is the body text that has the footnote attached to it
				      <text:note text:id="ftn0" text:note-class="footnote"><text:note-citation>1</text:note-citation><text:note-body><text:p>This is the footnote text itself. It is long so that a limit on the size can be seen in the document as it is presented. Note that the text following this means nothing, there are three copies of an ipsum paragraph, the last one is in it's own text:p element... Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</text:p><text:p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</text:p></text:note-body></text:note>
				   </text:p>
                </xsl:when>
                <xsl:when test="string-length($pageruby)!=0">
                  <text:p>
                    <text:ruby>
                      <text:ruby-base>
                        <text:span>新幹線</text:span>
                      </text:ruby-base>
                      <text:ruby-text>しんかんせん</text:ruby-text>
                    </text:ruby>
                  </text:p>
                </xsl:when>
                <xsl:when test="string-length($pagetable)!=0">
                  <table:table table:name="mytable" table:style-name="mytable">
                    <table:table-column table:number-columns-repeated="2" table:style-name="mytable.A"/>
                    <table:table-row>
                      <table:table-cell o:value-type="string" table:style-name="mytable.A1">
                        <text:p text:style-name="Table_20_Contents">First cell</text:p>
                      </table:table-cell>
                      <table:table-cell o:value-type="string" table:style-name="mytable.B1">
                        <text:p text:style-name="Table_20_Contents">Second cell</text:p>
                      </table:table-cell>
                    </table:table-row>
                    <table:table-row>
                      <table:table-cell o:value-type="string" table:style-name="mytable.A2">
                        <text:p text:style-name="Table_20_Contents">Third cell</text:p>
                      </table:table-cell>
                      <table:table-cell o:value-type="string" table:style-name="mytable.B2">
                        <text:p text:style-name="Table_20_Contents"/>
                      </table:table-cell>
                    </table:table-row>
                  </table:table>
                </xsl:when>
                <xsl:when test="string-length($fieldspage)!=0">
                  <text:p>Page: <text:page-number text:select-page="current">1000</text:page-number></text:p>
                </xsl:when>
                <xsl:when test="string-length($pagenumberedlist)!=0">
                  <text:list text:style-name="L1">
                    <text:list-item>
                      <text:p text:style-name="P2">first item</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P3">
                        <text:span text:style-name="T1">second item</text:span>
                      </text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P2">third item</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P4">4</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P4">5</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P4">6</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P4">7</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P4">8</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P4">9</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P4">10</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P4">11</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P4">12</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P4">13</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P4">14</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P4">15</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P4">16</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P4">17</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P4">18</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P4">19</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P4">20</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P4">21</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P4">22</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P4">23</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P4">24</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P4">25</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P4">26</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P4">27</text:p>
                    </text:list-item>
                    <text:list-item>
                      <text:p text:style-name="P4">28</text:p>
                    </text:list-item>
                  </text:list>
                </xsl:when>
                <xsl:when test="string-length($threepages)!=0">
                  <text:p>
                    <xsl:value-of select="$Loremipsum"/>
                  </text:p>
                  <text:p>
                    <xsl:value-of select="$Loremipsum"/>
                  </text:p>
                  <text:p>
                    <xsl:value-of select="$Loremipsum"/>
                  </text:p>
                </xsl:when>
                <xsl:when test="string-length($pageswithstyle)!=0">
                  <text:p>
	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				   </text:p>
                  <text:p>

    	But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?
				   </text:p>
                  <text:p>

    	On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammeled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.
				   </text:p>
                  <text:p>

    	But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?
				   </text:p>
                  <text:p>

    	On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammeled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.
				   </text:p>
                  <text:p>

    	But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?
				   </text:p>
                  <text:p>

    	On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammeled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.
				   </text:p>
                  <text:p>

    	But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?
				   </text:p>
                  <text:p>

    	On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammeled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.
				   </text:p>
                </xsl:when>
                <xsl:when test="string-length($threeshorttext)!=0">
                  <text:p>
                    <xsl:value-of select="$ipsum"/>
                  </text:p>
                  <text:p>
                    <xsl:value-of select="$ipsum"/>
                  </text:p>
                  <text:p>
                    <xsl:value-of select="$ipsum"/>
                  </text:p>
                </xsl:when>
                <xsl:when test="string-length($bodytext)!=0">
                  <text:p>
                    <xsl:value-of select="$bodytext"/>
                  </text:p>
                </xsl:when>
                <xsl:otherwise>
                  <text:p>hello world. HELLO WORLD.</text:p>
                </xsl:otherwise>
              </xsl:choose>
            </o:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:when test="$test='list'">
        <xsl:choose>
          <xsl:when test="$mode='ods'">
		</xsl:when>
          <xsl:when test="$mode='odp'">
		</xsl:when>
          <xsl:otherwise>
            <o:text>
              <text:list text:style-name="listStyle">
                <text:list-item>
                  <text:p>hello</text:p>
                </text:list-item>
                <text:list-item>
                  <text:p>world</text:p>
                </text:list-item>
                <text:list-item>
                  <text:p>end</text:p>
                </text:list-item>
              </text:list>
            </o:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$mode='ods'">
            <o:spreadsheet>
              <table:table table:name="TestTable" table:print="true" table:style-name="table">
                <table:table-column table:default-cell-style-name="style"/>
                <table:table-row>
                  <table:table-cell o:value-type="string">
                    <text:p>hello world. HELLO WORLD.</text:p>
                  </table:table-cell>
                </table:table-row>
              </table:table>
            </o:spreadsheet>
          </xsl:when>
          <xsl:when test="$mode='odp'">
            <o:presentation>
              <draw:page draw:master-page-name="Page">
                <draw:frame draw:style-name="style" svg:height="10cm" svg:width="10cm" svg:x="1cm" svg:y="1cm">
                  <draw:text-box>
                    <text:p>hello world. HELLO WORLD.</text:p>
                  </draw:text-box>
                </draw:frame>
              </draw:page>
            </o:presentation>
          </xsl:when>
          <xsl:otherwise>
            <o:text>
              <xsl:choose>
                <xsl:when test="string-length($threepages)!=0">
                  <text:p>
                    <xsl:value-of select="$Loremipsum"/>
                  </text:p>
                  <text:p text:style-name="style">
                    <xsl:value-of select="$Loremipsum"/>
                  </text:p>
                  <text:p>
                    <xsl:value-of select="$Loremipsum"/>
                  </text:p>
                </xsl:when>
                <xsl:when test="string-length($pageswithstyle)!=0">
                  <text:p text:style-name="style">
	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				   </text:p>
                  <text:p text:style-name="style">

    	But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?
				   </text:p>
                  <text:p text:style-name="style">

    	On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammeled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.
				   </text:p>
                  <text:p text:style-name="style">

    	But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?
				   </text:p>
                  <text:p text:style-name="style">

    	On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammeled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.
				   </text:p>
                  <text:p text:style-name="style">

    	But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?
				   </text:p>
                  <text:p text:style-name="style">

    	On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammeled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.
				   </text:p>
                  <text:p text:style-name="style">

    	But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?
				   </text:p>
                  <text:p text:style-name="style">

    	On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammeled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.
				   </text:p>
                </xsl:when>
                <xsl:when test="string-length($pageoblique)!=0">
                  <text:p text:style-name="style">The five boxing wizards jump quickly. This is in Style.</text:p>
                  <text:p text:style-name="italicstyle">The five boxing wizards jump quickly. This is in ItalicStyle.</text:p>
                </xsl:when>
                <xsl:when test="string-length($threeshorttext)!=0">
                  <text:p>
                    <xsl:value-of select="$ipsum"/>
                  </text:p>
                  <text:p text:style-name="style">
                    <xsl:value-of select="$ipsum"/>
                  </text:p>
                  <text:p>
                    <xsl:value-of select="$ipsum"/>
                  </text:p>
                </xsl:when>
                <xsl:when test="string-length($bodytext)!=0">
                  <text:p text:style-name="style">
                    <xsl:value-of select="$bodytext"/>
                  </text:p>
                </xsl:when>
                <xsl:otherwise>
                  <text:p text:style-name="style">hello world. HELLO WORLD.</text:p>
                </xsl:otherwise>
              </xsl:choose>
            </o:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="xpath">
    <xsl:param name="selector"/>
    <xsl:param name="value"/>
    <xsl:param name="index"/>
    <xsl:variable name="sel">
      <xsl:choose>
        <xsl:when test="$index = -1">
          <xsl:value-of select="$selector"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="concat('t:item(',$selector,',',$index,',&quot; &quot;)')"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="isLength">
      <xsl:call-template name="isLength">
        <xsl:with-param name="value" select="$value"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$isLength = 'true'">
        <xpath expr="t:compareLength({$sel},'{$value}')"/>
      </xsl:when>
      <xsl:otherwise>
        <xpath expr="{$sel}='{$value}'"/>
        <!-- second -->
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="xpaths">
    <xsl:param name="selector"/>
    <xsl:param name="value"/>
    <xsl:param name="index"/>
    <xsl:variable name="first" select="substring-before($value,' ')"/>
    <xsl:variable name="current">
      <xsl:choose>
        <xsl:when test="string-length($first) = 0">
          <xsl:value-of select="$value"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$first"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="i">
      <xsl:choose>
        <xsl:when test="string-length($first) != 0 and $index = -1">
          <xsl:value-of select="'0'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$index"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:call-template name="xpath">
      <xsl:with-param name="selector" select="$selector"/>
      <xsl:with-param name="value" select="$current"/>
      <xsl:with-param name="index" select="$i"/>
    </xsl:call-template>
    <xsl:if test="string-length($first)!=0">
      <xsl:call-template name="xpaths">
        <xsl:with-param name="selector" select="$selector"/>
        <xsl:with-param name="value" select="substring-after($value,' ')"/>
        <xsl:with-param name="index" select="$i + 1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
