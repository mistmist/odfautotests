<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns="http://www.example.org/documenttests" xmlns:anim="urn:oasis:names:tc:opendocument:xmlns:animation:1.0" xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0" xmlns:config="urn:oasis:names:tc:opendocument:xmlns:config:1.0" xmlns:db="urn:oasis:names:tc:opendocument:xmlns:database:1.0" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0" xmlns:manifest="urn:oasis:names:tc:opendocument:xmlns:manifest:1.0" xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0" xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datas:1.0" xmlns:o="urn:oasis:names:tc:opendocument:xmlns:office:1.0" xmlns:odf="http://docs.oasis-open.org/ns/office/1.2/meta/odf#" xmlns:of="urn:oasis:names:tc:opendocument:xmlns:of:1.2" xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0" xmlns:presentation="urn:oasis:names:tc:opendocument:xmlns:presentation:1.0" xmlns:s="urn:oasis:names:tc:opendocument:xmlns:style:1.0" xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0" xmlns:smil="urn:oasis:names:tc:opendocument:xmlns:smil-compatible:1.0" xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0" xmlns:t="http://www.example.org/documenttests" xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:import href="shared.xsl"/>
  <xsl:param name="mode" select="'odt'"/>
  <xsl:param name="test" select="'paragraph'"/>
  <xsl:param name="props" select="'paragraph'"/>
  <t:testtemplatesALL>
	</t:testtemplatesALL>
  <t:testtemplates>
    <t:testtemplate name="background-color">
      <fo:background-color value="#339999"/>
    </t:testtemplate>
    <t:testtemplate name="border">
      <fo:border value="2pt solid #000000"/>
    </t:testtemplate>
    <t:testtemplate name="border-bottom">
      <fo:border-bottom value="2pt solid #000000"/>
    </t:testtemplate>
    <t:testtemplate name="border-left">
      <fo:border-left value="2pt solid #000000"/>
    </t:testtemplate>
    <t:testtemplate name="border-right">
      <fo:border-right value="2pt solid #000000"/>
    </t:testtemplate>
    <t:testtemplate name="border-top">
      <fo:border-top value="2pt solid #000000"/>
    </t:testtemplate>
    <t:testtemplate name="margin">
      <fo:margin value="0.0299in"/>
    </t:testtemplate>
    <t:testtemplate name="margin-bottom">
      <fo:margin-bottom value="0.0299in"/>
    </t:testtemplate>
    <t:testtemplate name="margin-left">
      <fo:margin-left value="0.0299in"/>
    </t:testtemplate>
    <t:testtemplate name="margin-right">
      <fo:margin-right value="0.0299in"/>
    </t:testtemplate>
    <t:testtemplate name="margin-top">
      <fo:margin-top value="0.0299in"/>
    </t:testtemplate>
    <t:testtemplate name="min-height">
      <fo:min-height value="1in"/>
    </t:testtemplate>
    <t:testtemplate name="padding">
      <fo:padding value="0.0299in"/>
    </t:testtemplate>
    <t:testtemplate name="padding-bottom">
      <fo:padding-bottom value="0.0299in"/>
    </t:testtemplate>
    <t:testtemplate name="padding-left">
      <fo:padding-left value="0.0299in"/>
    </t:testtemplate>
    <t:testtemplate name="padding-right">
      <fo:padding-right value="0.0299in"/>
    </t:testtemplate>
    <t:testtemplate name="padding-top">
      <fo:padding-top value="0.0299in"/>
    </t:testtemplate>
    <t:testtemplate name="border-line-width">
      <fo:border value="5pt double #000000"/>
      <s:border-line-width value="0.4mm 0.0299in 0.7mm"/>
    </t:testtemplate>
    <t:testtemplate name="border-line-width-bottom">
      <fo:border-bottom value="5pt double #000000"/>
      <s:border-line-width-bottom value="0.4mm 0.0299in 0.7mm"/>
    </t:testtemplate>
    <t:testtemplate name="border-line-width-left">
      <fo:border-left value="5pt double #000000"/>
      <s:border-line-width-left value="0.4mm 0.0299in 0.7mm"/>
    </t:testtemplate>
    <t:testtemplate name="border-line-width-right">
      <fo:border-right value="5pt double #000000"/>
      <s:border-line-width-right value="0.4mm 0.0299in 0.7mm"/>
    </t:testtemplate>
    <t:testtemplate name="border-line-width-top">
      <fo:border-top value="5pt double #000000"/>
      <s:border-line-width-top value="0.4mm 0.0299in 0.7mm"/>
    </t:testtemplate>
    <t:testtemplate name="dynamic-spacing-true">
      <s:dynamic-spacing value="true"/>
    </t:testtemplate>
    <t:testtemplate name="dynamic-spacing-false">
      <s:dynamic-spacing value="false"/>
    </t:testtemplate>
    <t:testtemplate name="shadow">
      <s:shadow value="#808080 -0.0701in 0.0701in"/>
    </t:testtemplate>
    <t:testtemplate name="svg-height">
      <svg:height value="1in"/>
    </t:testtemplate>
  </t:testtemplates>
  <xsl:output encoding="utf-8" indent="no" method="xml" omit-xml-declaration="no"/>
  <xsl:template match="t:testtemplate">
    <xsl:variable name="family">
      <xsl:choose>
        <xsl:when test="$mode='ods'">
          <xsl:value-of select="'table-cell'"/>
        </xsl:when>
        <xsl:when test="$mode='odp'">
          <xsl:value-of select="'graphic'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'paragraph'"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <test name="{$mode}-{@name}">
      <input type="{$mode}1.2">
        <o:styles>
          <s:style s:family="paragraph" s:name="standard">
            <s:text-properties s:font-name="Helvetica" fo:font-size="12pt"/>
          </s:style>
          <s:style s:family="graphic" s:name="standard">
            <s:graphic-properties draw:fill="none" draw:stroke="none"/>
            <s:text-properties s:font-name="Helvetica" fo:font-size="12pt"/>
          </s:style>
          <s:style s:family="table-cell" s:name="standard">
            <s:text-properties s:font-name="Helvetica" fo:font-size="12pt"/>
          </s:style>
          <s:style s:display-name="Text" s:family="text" s:name="color">
            <s:text-properties fo:color="#339999"/>
          </s:style>
          <s:style s:display-name="paraTestStyle" s:family="{$family}" s:name="parastyle" s:parent-style-name="standard">
            <s:paragraph-properties>
						</s:paragraph-properties>
          </s:style>
          <s:style s:family="table" s:master-page-name="Standard" s:name="table">
            <s:table-properties s:writing-mode="lr-tb" table:display="true"/>
          </s:style>
          <s:style s:family="paragraph" s:name="plpstyle">
            <s:text-properties fo:color="#339999"/>
          </s:style>
        </o:styles>
        <o:automatic-styles>
          <s:page-layout s:name="Mpm1">
            <s:page-layout-properties s:footnote-max-height="0cm" s:num-format="1" s:print-orientation="portrait" s:writing-mode="lr-tb" fo:margin-bottom="2cm" fo:margin-left="2cm" fo:margin-right="2cm" fo:margin-top="2cm" fo:page-height="27.94cm" fo:page-width="21.59cm">
              <s:footnote-sep s:adjustment="left" s:color="#000000" s:distance-after-sep="0.101cm" s:distance-before-sep="0.101cm" s:line-style="solid" s:rel-width="25%" s:width="0.018cm"/>
            </s:page-layout-properties>
            <s:header-style>
              <s:header-footer-properties>
                <xsl:for-each select="*">
                  <xsl:attribute name="{name()}" namespace="{namespace-uri()}">
                    <xsl:value-of select="@value"/>
                  </xsl:attribute>
                </xsl:for-each>
              </s:header-footer-properties>
            </s:header-style>
            <s:footer-style>
              <s:header-footer-properties>
                <xsl:for-each select="*">
                  <xsl:attribute name="{name()}" namespace="{namespace-uri()}">
                    <xsl:value-of select="@value"/>
                  </xsl:attribute>
                </xsl:for-each>
              </s:header-footer-properties>
            </s:footer-style>
          </s:page-layout>
          <s:style s:family="table" s:master-page-name="Standard" s:name="table">
            <s:table-properties s:writing-mode="lr-tb" table:display="true"/>
          </s:style>
          <s:style s:family="paragraph" s:name="MP1" s:parent-style-name="Header">
            <s:text-properties/>
          </s:style>
          <s:style s:family="paragraph" s:name="MP2" s:parent-style-name="Footer">
            <s:text-properties/>
          </s:style>
        </o:automatic-styles>
        <o:master-styles>
          <s:master-page s:name="Standard" s:page-layout-name="Mpm1">
            <s:header>
              <text:p text:style-name="MP1">Header content here.</text:p>
            </s:header>
            <s:footer>
              <text:p text:style-name="MP2">Footer content here.</text:p>
            </s:footer>
          </s:master-page>
        </o:master-styles>
        <xsl:call-template name="body"/>
      </input>
      <output types="{$mode}1.0 {$mode}1.1 {$mode}1.2 {$mode}1.2ext">
        <file path="styles.xml">
          <xsl:for-each select="*">
            <xsl:variable name="selector" select="concat(&quot;//s:page-layout[1]/s:header-style/s:header-footer-properties/@&quot;,name())"/>
            <xpath expr="boolean({$selector})"/>
            <xsl:call-template name="xpaths">
              <xsl:with-param name="selector" select="$selector"/>
              <xsl:with-param name="value" select="@value"/>
              <xsl:with-param name="index" select="-1"/>
            </xsl:call-template>
          </xsl:for-each>
        </file>
      </output>
      <pdf/>
    </test>
  </xsl:template>
  <xsl:template match="/">
    <documenttests xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.example.org/documenttests ../documenttests.xsd">
      <xsl:variable name="tests" select="/xsl:stylesheet/t:testtemplates/*"/>
      <xsl:apply-templates select="$tests"/>
    </documenttests>
  </xsl:template>
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
