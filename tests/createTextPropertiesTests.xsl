<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns="http://www.example.org/documenttests" xmlns:anim="urn:oasis:names:tc:opendocument:xmlns:animation:1.0" xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0" xmlns:config="urn:oasis:names:tc:opendocument:xmlns:config:1.0" xmlns:db="urn:oasis:names:tc:opendocument:xmlns:database:1.0" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0" xmlns:manifest="urn:oasis:names:tc:opendocument:xmlns:manifest:1.0" xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0" xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0" xmlns:o="urn:oasis:names:tc:opendocument:xmlns:office:1.0" xmlns:odf="http://docs.oasis-open.org/ns/office/1.2/meta/odf#" xmlns:of="urn:oasis:names:tc:opendocument:xmlns:of:1.2" xmlns:presentation="urn:oasis:names:tc:opendocument:xmlns:presentation:1.0" xmlns:s="urn:oasis:names:tc:opendocument:xmlns:style:1.0" xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0" xmlns:smil="urn:oasis:names:tc:opendocument:xmlns:smil-compatible:1.0" xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0" xmlns:t="http://www.example.org/documenttests" xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:import href="shared.xsl"/>
  <xsl:param name="mode" select="'odt'"/>
  <xsl:param name="test" select="'paragraph'"/>
  <t:testtemplates>
    <t:testtemplate name="color">
      <t:test-style>
        <fo:color value="#339999"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="background-color">
      <t:test-style>
        <fo:background-color value="#339999"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="background-color-transparent">
      <t:middle-style>
        <fo:background-color value="#FF0000"/>
      </t:middle-style>
      <t:test-style>
        <fo:background-color value="transparent"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="country-language">
      <t:test-style>
        <fo:country value="NL"/>
        <fo:language value="nl"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-family">
      <t:middle-style>
        <fo:font-family value="Serif"/>
      </t:middle-style>
      <t:test-style>
        <fo:font-family value="Helvetica"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-size">
      <t:test-style>
        <fo:font-size value="6pt"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-size-perc">
      <t:middle-style>
        <fo:font-size value="6pt"/>
      </t:middle-style>
      <t:test-style>
        <fo:font-size value="200%"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-style-oblique-garamond">
      <t:test-style>
        <fo:font-style value="oblique"/>
        <fo:font-family value="Garamond"/>
      </t:test-style>
      <t:test-bodytext pageoblique="1"/>
    </t:testtemplate>
    <t:testtemplate name="font-style-oblique-helvetica">
      <t:test-style>
        <fo:font-style value="oblique"/>
        <fo:font-family value="Helvetica"/>
      </t:test-style>
      <t:test-bodytext pageoblique="1"/>
    </t:testtemplate>
    <t:testtemplate name="font-style-oblique-times">
      <t:test-style>
        <fo:font-style value="oblique"/>
        <fo:font-family value="Times"/>
      </t:test-style>
      <t:test-bodytext pageoblique="1"/>
    </t:testtemplate>
    <t:testtemplate name="font-style-normal">
      <t:middle-style>
        <fo:font-style value="italic"/>
      </t:middle-style>
      <t:test-style>
        <fo:font-style value="normal"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-variant-small-caps">
      <t:test-style>
        <fo:font-variant value="small-caps"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-variant-normal">
      <t:middle-style>
        <fo:font-variant value="small-caps"/>
      </t:middle-style>
      <t:test-style>
        <fo:font-variant value="normal"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-weight-bold">
      <t:test-style>
        <fo:font-weight value="bold"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-weight-100">
      <t:test-style>
        <fo:font-weight value="100"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-weight-500">
      <t:test-style>
        <fo:font-weight value="500"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-weight-900">
      <t:test-style>
        <fo:font-weight value="900"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="hyphenate-true">
      <t:test-style>
        <fo:hyphenate value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="hyphenate-false">
      <t:middle-style>
        <fo:hyphenate value="true"/>
      </t:middle-style>
      <t:test-style>
        <fo:hyphenate value="false"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="hyphenation-push-char-count-10">
      <t:test-style>
        <fo:hyphenation-push-char-count value="10"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="hyphenation-remain-char-count-10">
      <t:test-style>
        <fo:hyphenation-remain-char-count value="10"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="letter-spacing-normal">
      <t:middle-style>
        <fo:letter-spacing value="1px"/>
      </t:middle-style>
      <t:test-style>
        <fo:letter-spacing value="normal"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="letter-spacing-1mm">
      <t:test-style>
        <fo:letter-spacing value="1mm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="letter-spacing--1mm">
      <t:test-style>
        <fo:letter-spacing value="-1mm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="script">
      <t:test-style>
        <fo:script value="arab"/>
        <fo:country value="MA"/>
        <fo:language value="ar"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="script-hebr">
      <t:test-style>
        <fo:script value="hebr"/>
        <fo:country value="IL"/>
        <fo:language value="he"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-shadow">
      <t:test-style>
        <fo:text-shadow value="3pt 4pt"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-transform-none">
      <t:middle-style>
        <fo:text-transform value="uppercase"/>
      </t:middle-style>
      <t:test-style>
        <fo:text-transform value="none"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-transform-lowercase">
      <t:middle-style>
        <fo:text-transform value="uppercase"/>
      </t:middle-style>
      <t:test-style>
        <fo:text-transform value="lowercase"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-transform-uppercase">
      <t:middle-style>
        <fo:text-transform value="lowercase"/>
      </t:middle-style>
      <t:test-style>
        <fo:text-transform value="uppercase"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-transform-capitalize">
      <t:middle-style>
        <fo:text-transform value="lowercase"/>
      </t:middle-style>
      <t:test-style>
        <fo:text-transform value="capitalize"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="country-asian-jp">
      <t:test-style>
        <s:country-asian value="JP"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="country-complex">
      <t:test-style>
        <s:country-complex value="SA"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-charset-x-symbol">
      <t:test-style>
        <s:font-charset value="x-symbol"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-charset-utf8">
      <t:test-style>
        <s:font-charset value="utf8"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-charset-utf-8">
      <t:test-style>
        <s:font-charset value="utf-8"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-charset-asian">
      <t:test-style>
        <s:font-charset-asian value="x-symbol"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-charset-complex">
      <t:test-style>
        <s:font-charset-complex value="x-symbol"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-family-asian">
      <t:test-style>
        <s:font-family-asian value="Noto Sans JP Regular"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-family-complex">
      <t:test-style>
        <s:font-family-complex value="Helvetica"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-family-generic">
      <t:test-style>
        <s:font-family-generic value="modern"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-family-generic-asian">
      <t:test-style>
        <s:font-family-generic-asian value="modern"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-family-generic-complex">
      <t:test-style>
        <s:font-family-generic-complex value="modern"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-name">
      <t:middle-style>
        <fo:font-family value="Garamond"/>
      </t:middle-style>
      <t:test-style>
        <s:font-name value="Helvetica"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-name-asian">
      <t:test-style>
        <s:font-name-asian value="Noto Sans JP Regular"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-name-complex">
      <t:test-style>
        <s:font-name-complex value="Helvetica"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-pitch-fixed">
      <t:middle-style>
        <s:font-pitch value="variable"/>
      </t:middle-style>
      <t:test-style>
        <s:font-pitch value="fixed"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-pitch-variable">
      <t:middle-style>
        <s:font-pitch value="fixed"/>
      </t:middle-style>
      <t:test-style>
        <s:font-pitch value="variable"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-pitch-asian">
      <t:test-style>
        <s:font-pitch-asian value="variable"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-pitch-complex">
      <t:test-style>
        <s:font-pitch-complex value="variable"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-relief-none">
      <t:middle-style>
        <s:font-relief value="engraved"/>
      </t:middle-style>
      <t:test-style>
        <s:font-relief value="none"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-relief-embossed">
      <t:test-style>
        <s:font-relief value="embossed"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-relief-engraved">
      <t:test-style>
        <s:font-relief value="engraved"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-size-asian">
      <t:test-style>
        <s:font-size-asian value="3pt"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-size-complex">
      <t:test-style>
        <s:font-size-complex value="3pt"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-size-rel">
      <t:test-style>
        <s:font-size-rel value="16pt"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-size-rel-asian">
      <t:test-style>
        <s:font-size-rel-asian value="16pt"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-size-rel-complex">
      <t:test-style>
        <s:font-size-rel-complex value="16pt"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-style-asian">
      <t:test-style>
        <s:font-style-asian value="italic"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-style-complex">
      <t:test-style>
        <s:font-style-complex value="italic"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-style-name">
      <t:test-style>
        <s:font-style-name value="Bold"/>
        <fo:font-family value="Helvetica"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-style-name-asian">
      <t:test-style>
        <s:font-style-name-asian value="Bold"/>
        <fo:font-family value="Helvetica"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-style-name-complex">
      <t:test-style>
        <s:font-style-name-complex value="Bold"/>
        <fo:font-family value="Helvetica"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-weight-asian">
      <t:test-style>
        <s:font-weight-asian value="bold"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-weight-complex">
      <t:test-style>
        <s:font-weight-complex value="bold"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="language-asian">
      <t:test-style>
        <s:language-asian value="ja"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="language-complex">
      <t:middle-style>
        <s:language-complex value="hi"/>
        <s:country-complex value="IN"/>
      </t:middle-style>
      <t:test-style>
        <s:language-complex value="ar"/>
        <s:country-complex value="SA"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="letter-kerning-false">
      <t:middle-style>
        <s:letter-kerning value="true"/>
      </t:middle-style>
      <t:test-style>
        <s:letter-kerning value="false"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="letter-kerning-true">
      <t:middle-style>
        <s:letter-kerning value="false"/>
      </t:middle-style>
      <t:test-style>
        <s:letter-kerning value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="rfc-language-tag">
      <t:test-style>
        <s:rfc-language-tag value="mn"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="rfc-language-tag-asian">
      <t:test-style>
        <s:rfc-language-tag-asian value="mn"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="rfc-language-tag-complex">
      <t:test-style>
        <s:rfc-language-tag-complex value="mn"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="script-asian">
      <t:test-style>
        <s:script-asian value="en"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="script-complex">
      <t:test-style>
        <s:script-complex value="en"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="script-type-latin">
      <t:test-style>
        <s:script-type value="latin"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="script-type-asian">
      <t:test-style>
        <s:script-type value="latin"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-blinking">
      <t:test-style>
        <s:text-blinking value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-combine-none">
      <t:middle-style>
        <s:text-combine value="letters"/>
      </t:middle-style>
      <t:test-style>
        <s:text-combine value="none"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-combine-letters">
      <t:test-style>
        <s:text-combine value="letters"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-combine-lines">
      <t:test-style>
        <s:text-combine value="lines"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-combine-end-char">
      <t:test-style>
        <s:text-combine-end-char value="Z"/>
        <s:text-combine value="lines"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-combine-start-char">
      <t:test-style>
        <s:text-combine-start-char value="A"/>
        <s:text-combine-end-char value="Z"/>
        <s:text-combine value="lines"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-emphasize">
      <t:test-style>
        <s:text-emphasize value="disc below"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-line-through-color">
      <t:test-style>
        <s:text-line-through-color value="#339999"/>
        <s:text-line-through-style value="solid"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-line-through-color-font">
      <t:test-style>
        <s:text-line-through-color value="font-color"/>
        <s:text-line-through-style value="solid"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-line-through-mode-continuous">
      <t:test-style>
        <s:text-line-through-mode value="continuous"/>
        <s:text-line-through-style value="solid"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-line-through-mode-skip-white-space">
      <t:test-style>
        <s:text-line-through-mode value="skip-white-space"/>
        <s:text-line-through-style value="solid"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-line-through-style-solid">
      <t:test-style>
        <s:text-line-through-style value="solid"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-line-through-style-wave">
      <t:test-style>
        <s:text-line-through-style value="wave"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-line-through-text">
      <t:test-style>
        <s:text-line-through-style value="solid"/>
        <s:text-line-through-text value="X"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-line-through-text-style">
      <t:test-style>
        <s:text-line-through-text-style value="color"/>
        <s:text-line-through-text value="X"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-line-through-type-none">
      <t:middle-style>
        <s:text-line-through-type value="double"/>
      </t:middle-style>
      <t:test-style>
        <s:text-line-through-type value="none"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-line-through-type-single">
      <t:test-style>
        <s:text-line-through-type value="single"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-line-through-type-double">
      <t:test-style>
        <s:text-line-through-type value="double"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-line-through-width-auto">
      <t:test-style>
        <s:text-line-through-width value="auto"/>
        <s:text-line-through-style value="solid"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-line-through-width-bold">
      <t:test-style>
        <s:text-line-through-width value="bold"/>
        <s:text-line-through-style value="solid"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-line-through-width-thin">
      <t:test-style>
        <s:text-line-through-width value="thin"/>
        <s:text-line-through-style value="solid"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-outline-true">
      <t:test-style>
        <s:text-outline value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-outline-false">
      <t:middle-style>
        <s:text-outline value="true"/>
      </t:middle-style>
      <t:test-style>
        <s:text-outline value="false"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-overline-color">
      <t:test-style>
        <s:text-overline-color value="#339999"/>
        <s:text-overline-style value="solid"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-overline-mode">
      <t:test-style>
        <s:text-overline-mode value="continuous"/>
        <s:text-overline-style value="solid"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-overline-style">
      <t:test-style>
        <s:text-overline-style value="solid"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-overline-type">
      <t:test-style>
        <s:text-overline-type value="single"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-overline-width">
      <t:test-style>
        <s:text-overline-width value="1pt"/>
        <s:text-overline-style value="solid"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-position-sub">
      <t:test-style>
        <s:text-position value="sub"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-position-sub-50">
      <t:test-style>
        <s:text-position value="sub 50%"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-position-super">
      <t:test-style>
        <s:text-position value="super"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-position-super-50">
      <t:test-style>
        <s:text-position value="super 50%"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-rotation-angle">
      <t:test-style>
        <s:text-rotation-angle value="90"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-rotation-scale-fixed">
      <t:test-style>
        <s:text-rotation-scale value="fixed"/>
        <s:text-rotation-angle value="90"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-rotation-scale-line-height">
      <t:test-style>
        <s:text-rotation-scale value="line-height"/>
        <s:text-rotation-angle value="90"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-scale">
      <t:test-style>
        <s:text-scale value="50%"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-scale-wide">
      <t:test-style>
        <s:text-scale value="200%"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-underline-color-font-color">
      <t:middle-style>
        <s:text-underline-color value="#FF0000"/>
        <s:text-underline-style value="dotted"/>
      </t:middle-style>
      <t:test-style>
        <s:text-underline-color value="font-color"/>
        <s:text-underline-style value="solid"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-underline-color">
      <t:test-style>
        <s:text-underline-color value="#339999"/>
        <s:text-underline-style value="solid"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-underline-mode-continuous">
      <t:test-style>
        <s:text-underline-mode value="continuous"/>
        <s:text-underline-style value="solid"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-underline-mode-skip-white-space">
      <t:test-style>
        <s:text-underline-mode value="skip-white-space"/>
        <s:text-underline-style value="solid"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-underline-style-none">
      <t:middle-style>
        <s:text-underline-style value="dash"/>
      </t:middle-style>
      <t:test-style>
        <s:text-underline-style value="none"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-underline-style-dash">
      <t:test-style>
        <s:text-underline-style value="dash"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-underline-style-solid">
      <t:test-style>
        <s:text-underline-style value="solid"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-underline-style-wave">
      <t:test-style>
        <s:text-underline-style value="wave"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-underline-type-none">
      <t:middle-style>
        <s:text-underline-type value="double"/>
      </t:middle-style>
      <t:test-style>
        <s:text-underline-type value="none"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-underline-type-single">
      <t:test-style>
        <s:text-underline-type value="single"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-underline-type-double">
      <t:test-style>
        <s:text-underline-type value="double"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-underline-width">
      <t:middle-style>
        <s:text-underline-width value="2pt"/>
        <s:text-underline-style value="dash"/>
      </t:middle-style>
      <t:test-style>
        <s:text-underline-width value="1pt"/>
        <s:text-underline-style value="solid"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="use-window-font-color-true">
      <t:middle-style>
        <s:use-window-font-color value="false"/>
      </t:middle-style>
      <t:test-style>
        <s:use-window-font-color value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="use-window-font-color-false">
      <t:middle-style>
        <s:use-window-font-color value="true"/>
      </t:middle-style>
      <t:test-style>
        <s:use-window-font-color value="false"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="display-true">
      <t:middle-style>
        <text:display value="none"/>
      </t:middle-style>
      <t:test-style>
        <text:display value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="display-none">
      <t:test-style>
        <text:display value="none"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="display-condition">
      <t:test-style>
        <text:display value="condition"/>
        <text:condition value="none"/>
      </t:test-style>
    </t:testtemplate>
  </t:testtemplates>
  <xsl:output encoding="utf-8" indent="no" method="xml" omit-xml-declaration="no"/>
  <xsl:template match="t:testtemplate">
    <xsl:variable name="family">
      <xsl:choose>
        <xsl:when test="$mode='ods'">
          <xsl:value-of select="'table-cell'"/>
        </xsl:when>
        <xsl:when test="$mode='odp'">
          <xsl:value-of select="'graphic'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'paragraph'"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <test name="{$mode}-{@name}">
      <input type="{$mode}1.2">
        <o:styles>
          <s:style s:family="paragraph" s:name="standard">
            <s:text-properties s:font-name="Helvetica" fo:font-size="12pt"/>
          </s:style>
          <s:style s:family="graphic" s:name="standard">
            <s:graphic-properties draw:fill="none" draw:stroke="none"/>
            <s:text-properties s:font-name="Helvetica" fo:font-size="12pt"/>
          </s:style>
          <s:style s:family="table-cell" s:name="standard">
            <s:text-properties s:font-name="Helvetica" fo:font-size="12pt"/>
          </s:style>
          <s:style s:display-name="Text" s:family="text" s:name="color">
            <s:text-properties fo:color="#339999"/>
          </s:style>
          <s:style s:display-name="middle" s:family="{$family}" s:name="middle" s:parent-style-name="standard">
            <s:text-properties>
              <xsl:for-each select="t:middle-style/*">
                <xsl:attribute name="{name()}" namespace="{namespace-uri()}">
                  <xsl:value-of select="@value"/>
                </xsl:attribute>
              </xsl:for-each>
            </s:text-properties>
          </s:style>
          <s:style s:display-name="TestStyle" s:family="{$family}" s:name="style" s:parent-style-name="middle">
            <s:text-properties>
              <xsl:for-each select="t:test-style/*">
                <xsl:attribute name="{name()}" namespace="{namespace-uri()}">
                  <xsl:value-of select="@value"/>
                </xsl:attribute>
              </xsl:for-each>
            </s:text-properties>
          </s:style>
          <s:style s:display-name="italicstyle" s:family="{$family}" s:name="italicstyle" s:parent-style-name="style">
            <s:text-properties fo:font-style="italic"/>
          </s:style>
          <s:style s:family="table" s:master-page-name="Standard" s:name="table">
            <s:table-properties s:writing-mode="lr-tb" table:display="true"/>
          </s:style>
        </o:styles>
        <o:automatic-styles>
          <s:style s:family="table" s:master-page-name="Standard" s:name="table">
            <s:table-properties s:writing-mode="lr-tb" table:display="true"/>
          </s:style>
        </o:automatic-styles>
        <xsl:choose>
          <xsl:when test="t:test-bodytext[@pageoblique]">
            <xsl:call-template name="body">
              <xsl:with-param name="pageoblique" select="1"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name="body">
              <xsl:with-param name="bodytext" select="concat('',t:test-bodytext)"/>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </input>
      <output types="{$mode}1.0 {$mode}1.1 {$mode}1.2 {$mode}1.2ext">
        <file path="styles.xml">
          <xsl:for-each select="t:test-style/*">
            <xsl:variable name="selector" select="concat(&quot;//s:style[@s:display-name='TestStyle' or (not(@s:display-name) and @s:name='TestStyle')]/s:text-properties/@&quot;,name())"/>
            <xpath expr="boolean({$selector})"/>
            <xsl:call-template name="xpaths">
              <xsl:with-param name="selector" select="$selector"/>
              <xsl:with-param name="value" select="@value"/>
              <xsl:with-param name="index" select="-1"/>
            </xsl:call-template>
          </xsl:for-each>
        </file>
      </output>
      <pdf/>
    </test>
  </xsl:template>
  <xsl:template match="/">
    <documenttests xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.example.org/documenttests ../documenttests.xsd">
      <xsl:variable name="tests" select="/xsl:stylesheet/t:testtemplates/*"/>
      <xsl:apply-templates select="$tests"/>
    </documenttests>
  </xsl:template>
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
