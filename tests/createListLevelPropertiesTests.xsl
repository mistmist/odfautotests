<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns="http://www.example.org/documenttests" xmlns:anim="urn:oasis:names:tc:opendocument:xmlns:animation:1.0" xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0" xmlns:config="urn:oasis:names:tc:opendocument:xmlns:config:1.0" xmlns:db="urn:oasis:names:tc:opendocument:xmlns:database:1.0" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0" xmlns:manifest="urn:oasis:names:tc:opendocument:xmlns:manifest:1.0" xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0" xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datas:1.0" xmlns:o="urn:oasis:names:tc:opendocument:xmlns:office:1.0" xmlns:odf="http://docs.oasis-open.org/ns/office/1.2/meta/odf#" xmlns:of="urn:oasis:names:tc:opendocument:xmlns:of:1.2" xmlns:presentation="urn:oasis:names:tc:opendocument:xmlns:presentation:1.0" xmlns:s="urn:oasis:names:tc:opendocument:xmlns:style:1.0" xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0" xmlns:smil="urn:oasis:names:tc:opendocument:xmlns:smil-compatible:1.0" xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0" xmlns:t="http://www.example.org/documenttests" xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:import href="shared.xsl"/>
  <xsl:param name="mode" select="'odt'"/>
  <xsl:param name="test" select="'list'"/>
  <xsl:param name="props" select="'paragraph'"/>
  <t:testtemplates>
    <t:testtemplate name="height">
      <fo:height value="3pt"/>
    </t:testtemplate>
    <t:testtemplate name="text-align-center">
      <fo:text-align value="center"/>
    </t:testtemplate>
    <t:testtemplate name="text-align-right">
      <fo:text-align value="right"/>
    </t:testtemplate>
    <t:testtemplate name="width">
      <fo:width value="3pt"/>
    </t:testtemplate>
    <t:testtemplate name="font-name">
      <s:font-name value="Helvetica"/>
    </t:testtemplate>
    <t:testtemplate name="vertical-pos-bottom">
      <s:vertical-pos value="bottom"/>
    </t:testtemplate>
    <t:testtemplate name="vertical-pos-middle">
      <s:vertical-pos value="middle"/>
    </t:testtemplate>
    <t:testtemplate name="vertical-rel">
      <s:vertical-rel value="paragraph"/>
    </t:testtemplate>
    <t:testtemplate name="svg-y">
      <svg:y value="2pt"/>
    </t:testtemplate>
    <t:testtemplate name="list-level-position-and-space-mode">
      <text:list-level-position-and-space-mode value="label-width-and-position"/>
    </t:testtemplate>
    <t:testtemplate name="min-label-distance">
      <text:min-label-distance value="5pt"/>
    </t:testtemplate>
    <t:testtemplate name="min-label-width">
      <text:min-label-width value="7pt"/>
    </t:testtemplate>
    <t:testtemplate name="space-before">
      <text:space-before value="2pt"/>
    </t:testtemplate>
  </t:testtemplates>
  <xsl:output encoding="utf-8" indent="no" method="xml" omit-xml-declaration="no"/>
  <xsl:template match="t:testtemplate">
    <xsl:variable name="family">
      <xsl:choose>
        <xsl:when test="$mode='ods'">
          <xsl:value-of select="'table-cell'"/>
        </xsl:when>
        <xsl:when test="$mode='odp'">
          <xsl:value-of select="'graphic'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'paragraph'"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <test name="{$mode}-{@name}">
      <input type="{$mode}1.2">
        <o:styles>
          <s:style s:family="paragraph" s:name="standard">
            <s:text-properties s:font-name="Helvetica" fo:font-size="12pt"/>
          </s:style>
          <s:style s:family="graphic" s:name="standard">
            <s:graphic-properties draw:fill="none" draw:stroke="none"/>
            <s:text-properties s:font-name="Helvetica" fo:font-size="12pt"/>
          </s:style>
          <s:style s:family="table-cell" s:name="standard">
            <s:text-properties s:font-name="Helvetica" fo:font-size="12pt"/>
          </s:style>
          <s:style s:display-name="Text" s:family="text" s:name="color">
            <s:text-properties fo:color="#339999"/>
          </s:style>
          <s:style s:display-name="TestStyle" s:family="{$family}" s:name="style" s:parent-style-name="standard">
            <s:paragraph-properties>
              <!--
                                                        <xsl:for-each select="*">
								<xsl:attribute namespace="{namespace-uri()}" name="{name()}"><xsl:value-of
									select="@value" /></xsl:attribute>
							</xsl:for-each>
 -->
            </s:paragraph-properties>
          </s:style>
          <s:style s:family="table" s:master-page-name="Standard" s:name="table">
            <s:table-properties s:writing-mode="lr-tb" table:display="true"/>
          </s:style>
          <text:list-style s:name="listStyle">
            <text:list-level-style-number s:num-format="1" s:num-suffix="." text:level="1" text:style-name="Numbering_20_Symbols">
              <s:list-level-properties>
                <xsl:for-each select="*">
                  <xsl:attribute name="{name()}" namespace="{namespace-uri()}">
                    <xsl:value-of select="@value"/>
                  </xsl:attribute>
                </xsl:for-each>
                <s:list-level-label-alignment text:label-followed-by="listtab" text:list-tab-stop-position="1.27cm" fo:margin-left="1.27cm" fo:text-indent="-0.635cm"/>
              </s:list-level-properties>
            </text:list-level-style-number>
          </text:list-style>
        </o:styles>
        <o:automatic-styles>
          <s:style s:family="table" s:master-page-name="Standard" s:name="table">
            <s:table-properties s:writing-mode="lr-tb" table:display="true"/>
          </s:style>
        </o:automatic-styles>
        <xsl:call-template name="body"/>
      </input>
      <output types="{$mode}1.0 {$mode}1.1 {$mode}1.2 {$mode}1.2ext">
        <file path="styles.xml">
          <xsl:for-each select="*">
            <xsl:variable name="selector" select="concat(&quot;//text:list-style[@s:display-name='listStyle' or (not(@s:display-name) and @s:name='listStyle')]/text:list-level-style-number/s:list-level-properties/@&quot;,name())"/>
            <xpath expr="boolean({$selector})"/>
            <xsl:call-template name="xpaths">
              <xsl:with-param name="selector" select="$selector"/>
              <xsl:with-param name="value" select="@value"/>
              <xsl:with-param name="index" select="-1"/>
            </xsl:call-template>
          </xsl:for-each>
        </file>
      </output>
      <pdf/>
    </test>
  </xsl:template>
  <xsl:template match="/">
    <documenttests xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.example.org/documenttests ../documenttests.xsd">
      <xsl:variable name="tests" select="/xsl:stylesheet/t:testtemplates/*"/>
      <xsl:apply-templates select="$tests"/>
    </documenttests>
  </xsl:template>
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
