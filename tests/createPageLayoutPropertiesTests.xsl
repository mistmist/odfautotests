<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns="http://www.example.org/documenttests" xmlns:anim="urn:oasis:names:tc:opendocument:xmlns:animation:1.0" xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0" xmlns:config="urn:oasis:names:tc:opendocument:xmlns:config:1.0" xmlns:db="urn:oasis:names:tc:opendocument:xmlns:database:1.0" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0" xmlns:manifest="urn:oasis:names:tc:opendocument:xmlns:manifest:1.0" xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0" xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datas:1.0" xmlns:o="urn:oasis:names:tc:opendocument:xmlns:office:1.0" xmlns:odf="http://docs.oasis-open.org/ns/office/1.2/meta/odf#" xmlns:of="urn:oasis:names:tc:opendocument:xmlns:of:1.2" xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0" xmlns:presentation="urn:oasis:names:tc:opendocument:xmlns:presentation:1.0" xmlns:s="urn:oasis:names:tc:opendocument:xmlns:style:1.0" xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0" xmlns:smil="urn:oasis:names:tc:opendocument:xmlns:smil-compatible:1.0" xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0" xmlns:t="http://www.example.org/documenttests" xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:import href="shared.xsl"/>
  <xsl:param name="mode" select="'odt'"/>
  <xsl:param name="test" select="'page'"/>
  <xsl:param name="props" select="'paragraph'"/>
  <t:testtemplatesALL>
	</t:testtemplatesALL>
  <t:testtemplates>
    <t:testtemplate name="background-color">
      <t:test-style>
        <fo:background-color value="#339999"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border">
      <t:test-style>
        <fo:border value="2pt solid #000000"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border-bottom">
      <t:test-style>
        <fo:border-bottom value="2pt solid #000000"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border-left">
      <t:test-style>
        <fo:border-left value="2pt solid #000000"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border-right">
      <t:test-style>
        <fo:border-right value="2pt solid #000000"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border-top">
      <t:test-style>
        <fo:border-top value="2pt solid #000000"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="margin">
      <t:test-style>
        <fo:margin value="2in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="margin-bottom">
      <t:test-style>
        <fo:margin-bottom value="1in"/>
      </t:test-style>
      <t:test-bodytext threepages="1"/>
    </t:testtemplate>
    <t:testtemplate name="margin-left">
      <t:test-style>
        <fo:margin-left value="1in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="margin-right">
      <t:test-style>
        <fo:margin-right value="1in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="margin-top">
      <t:test-style>
        <fo:margin-top value="1in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="padding">
      <t:test-style>
        <fo:padding value="1.5in"/>
      </t:test-style>
      <t:test-bodytext threepages="1"/>
    </t:testtemplate>
    <t:testtemplate name="padding-bottom">
      <t:test-style>
        <fo:padding-bottom value="4.5in"/>
      </t:test-style>
      <t:test-bodytext threepages="1"/>
    </t:testtemplate>
    <t:testtemplate name="padding-left">
      <t:test-style>
        <fo:padding-left value="1.5in"/>
      </t:test-style>
      <t:test-bodytext threepages="1"/>
    </t:testtemplate>
    <t:testtemplate name="padding-right">
      <t:test-style>
        <fo:padding-right value="1.5in"/>
      </t:test-style>
      <t:test-bodytext threepages="1"/>
    </t:testtemplate>
    <t:testtemplate name="padding-top">
      <t:test-style>
        <fo:padding-top value="1.5in"/>
      </t:test-style>
      <t:test-bodytext threepages="1"/>
    </t:testtemplate>
    <t:testtemplate name="page-height-cm">
      <t:test-style>
        <fo:page-height value="10cm"/>
      </t:test-style>
      <t:test-bodytext threepages="1"/>
    </t:testtemplate>
    <t:testtemplate name="page-width-cm">
      <t:test-style>
        <fo:page-width value="10cm"/>
      </t:test-style>
      <t:test-bodytext threepages="1"/>
    </t:testtemplate>
    <t:testtemplate name="page-height-in">
      <t:test-style>
        <fo:page-height value="4in"/>
      </t:test-style>
      <t:test-bodytext threepages="1"/>
    </t:testtemplate>
    <t:testtemplate name="page-width-in">
      <t:test-style>
        <fo:page-width value="4in"/>
      </t:test-style>
      <t:test-bodytext threepages="1"/>
    </t:testtemplate>
    <t:testtemplate name="border-line-width">
      <t:test-style>
        <fo:border value="5pt double #000000"/>
        <s:border-line-width value="0.4mm 0.0299in 0.7mm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border-line-width-bottom">
      <t:test-style>
        <fo:border-bottom value="5pt double #000000"/>
        <s:border-line-width-bottom value="0.4mm 0.0299in 0.7mm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border-line-width-left">
      <t:test-style>
        <fo:border-left value="5pt double #000000"/>
        <s:border-line-width-left value="0.4mm 0.0299in 0.7mm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border-line-width-right">
      <t:test-style>
        <fo:border-right value="5pt double #000000"/>
        <s:border-line-width-right value="0.4mm 0.0299in 0.7mm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border-line-width-top">
      <t:test-style>
        <fo:border-top value="5pt double #000000"/>
        <s:border-line-width-top value="0.4mm 0.0299in 0.7mm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="first-page-number">
      <t:test-style>
        <s:first-page-number value="15"/>
      </t:test-style>
      <t:test-bodytext fieldspage="1"/>
    </t:testtemplate>
    <t:testtemplate name="footnote-max-height-in">
      <t:test-style>
        <s:footnote-max-height value="1in"/>
      </t:test-style>
      <t:test-bodytext pagefootnote="1"/>
    </t:testtemplate>
    <t:testtemplate name="footnote-max-height-pt">
      <t:test-style>
        <s:footnote-max-height value="30pt"/>
      </t:test-style>
      <t:test-bodytext pagefootnote="1"/>
    </t:testtemplate>
    <t:testtemplate name="layout-grid-base-height">
      <t:test-style>
        <s:layout-grid-base-height value="15pt"/>
        <s:layout-grid-color value="#c0c0c0"/>
        <s:layout-grid-lines value="15"/>
        <s:layout-grid-mode value="both"/>
        <s:layout-grid-ruby-below value="false"/>
        <s:layout-grid-print value="true"/>
        <s:layout-grid-display value="true"/>
      </t:test-style>
      <t:test-bodytext>ソビエト連邦のルナ計画により、1959年1月4日に打ち上げられたルナ1号（E-1）は、月の近隣を通過した史上初の月探査機となった。その後継機であるルナ2号は月面に達した。1959年10月7日には、ルナ3号が月の裏側の写真撮影に成功した。その後1966年2月3日に連邦の打ち上げたルナ9号が初の月面「軟着陸」を成し遂げると、1966年4月3日にはルナ10号が月の衛星となることに成功したのであった。</t:test-bodytext>
    </t:testtemplate>
    <t:testtemplate name="layout-grid-base-width">
      <t:test-style>
        <s:layout-grid-base-width value="15pt"/>
        <s:layout-grid-color value="#c0c0c0"/>
        <s:layout-grid-lines value="15"/>
        <s:layout-grid-mode value="both"/>
        <s:layout-grid-ruby-below value="false"/>
        <s:layout-grid-print value="true"/>
        <s:layout-grid-display value="true"/>
      </t:test-style>
      <t:test-bodytext>ソビエト連邦のルナ計画により、1959年1月4日に打ち上げられたルナ1号（E-1）は、月の近隣を通過した史上初の月探査機となった。その後継機であるルナ2号は月面に達した。1959年10月7日には、ルナ3号が月の裏側の写真撮影に成功した。その後1966年2月3日に連邦の打ち上げたルナ9号が初の月面「軟着陸」を成し遂げると、1966年4月3日にはルナ10号が月の衛星となることに成功したのであった。</t:test-bodytext>
    </t:testtemplate>
    <t:testtemplate name="layout-grid-color">
      <t:test-style>
        <s:layout-grid-color value="#ff0000"/>
        <s:layout-grid-lines value="15"/>
        <s:layout-grid-mode value="both"/>
        <s:layout-grid-ruby-below value="false"/>
        <s:layout-grid-print value="true"/>
        <s:layout-grid-display value="true"/>
      </t:test-style>
      <t:test-bodytext>ソビエト連邦のルナ計画により、1959年1月4日に打ち上げられたルナ1号（E-1）は、月の近隣を通過した史上初の月探査機となった。その後継機であるルナ2号は月面に達した。1959年10月7日には、ルナ3号が月の裏側の写真撮影に成功した。その後1966年2月3日に連邦の打ち上げたルナ9号が初の月面「軟着陸」を成し遂げると、1966年4月3日にはルナ10号が月の衛星となることに成功したのであった。</t:test-bodytext>
    </t:testtemplate>
    <t:testtemplate name="layout-grid-display">
      <t:test-style>
        <s:layout-grid-display value="true"/>
        <s:layout-grid-color value="#c0c0c0"/>
        <s:layout-grid-lines value="15"/>
        <s:layout-grid-mode value="both"/>
        <s:layout-grid-ruby-below value="false"/>
        <s:layout-grid-print value="true"/>
      </t:test-style>
      <t:test-bodytext>ソビエト連邦のルナ計画により、1959年1月4日に打ち上げられたルナ1号（E-1）は、月の近隣を通過した史上初の月探査機となった。その後継機であるルナ2号は月面に達した。1959年10月7日には、ルナ3号が月の裏側の写真撮影に成功した。その後1966年2月3日に連邦の打ち上げたルナ9号が初の月面「軟着陸」を成し遂げると、1966年4月3日にはルナ10号が月の衛星となることに成功したのであった。</t:test-bodytext>
    </t:testtemplate>
    <t:testtemplate name="layout-grid-lines">
      <t:test-style>
        <s:layout-grid-lines value="10"/>
        <s:layout-grid-color value="#c0c0c0"/>
        <s:layout-grid-mode value="both"/>
        <s:layout-grid-ruby-below value="false"/>
        <s:layout-grid-print value="true"/>
        <s:layout-grid-display value="true"/>
      </t:test-style>
      <t:test-bodytext>ソビエト連邦のルナ計画により、1959年1月4日に打ち上げられたルナ1号（E-1）は、月の近隣を通過した史上初の月探査機となった。その後継機であるルナ2号は月面に達した。1959年10月7日には、ルナ3号が月の裏側の写真撮影に成功した。その後1966年2月3日に連邦の打ち上げたルナ9号が初の月面「軟着陸」を成し遂げると、1966年4月3日にはルナ10号が月の衛星となることに成功したのであった。</t:test-bodytext>
    </t:testtemplate>
    <t:testtemplate name="layout-grid-mode-both">
      <t:test-style>
        <s:layout-grid-mode value="both"/>
        <s:layout-grid-color value="#c0c0c0"/>
        <s:layout-grid-lines value="15"/>
        <s:layout-grid-ruby-below value="false"/>
        <s:layout-grid-print value="true"/>
        <s:layout-grid-display value="true"/>
      </t:test-style>
      <t:test-bodytext>ソビエト連邦のルナ計画により、1959年1月4日に打ち上げられたルナ1号（E-1）は、月の近隣を通過した史上初の月探査機となった。その後継機であるルナ2号は月面に達した。1959年10月7日には、ルナ3号が月の裏側の写真撮影に成功した。その後1966年2月3日に連邦の打ち上げたルナ9号が初の月面「軟着陸」を成し遂げると、1966年4月3日にはルナ10号が月の衛星となることに成功したのであった。</t:test-bodytext>
    </t:testtemplate>
    <t:testtemplate name="layout-grid-mode-lines">
      <t:test-style>
        <s:layout-grid-mode value="line"/>
        <s:layout-grid-color value="#c0c0c0"/>
        <s:layout-grid-lines value="15"/>
        <s:layout-grid-ruby-below value="false"/>
        <s:layout-grid-print value="true"/>
        <s:layout-grid-display value="true"/>
      </t:test-style>
      <t:test-bodytext>ソビエト連邦のルナ計画により、1959年1月4日に打ち上げられたルナ1号（E-1）は、月の近隣を通過した史上初の月探査機となった。その後継機であるルナ2号は月面に達した。1959年10月7日には、ルナ3号が月の裏側の写真撮影に成功した。その後1966年2月3日に連邦の打ち上げたルナ9号が初の月面「軟着陸」を成し遂げると、1966年4月3日にはルナ10号が月の衛星となることに成功したのであった。</t:test-bodytext>
    </t:testtemplate>
    <!-- not testable?
		<t:testtemplate name="layout-grid-print">
			<t:test-style>
			<s:layout-grid-print value="true" />

				    <s:layout-grid-color value="#c0c0c0" />
				    <s:layout-grid-lines value="15" />
				    <s:layout-grid-mode value="both" />
				    <s:layout-grid-ruby-below value="false" />
				    <s:layout-grid-display value="true" />

			</t:test-style>
		</t:testtemplate>
-->
    <t:testtemplate name="layout-grid-ruby-below">
      <t:test-style>
        <s:layout-grid-ruby-below value="true"/>
        <s:layout-grid-color value="#c0c0c0"/>
        <s:layout-grid-lines value="15"/>
        <s:layout-grid-mode value="both"/>
        <s:layout-grid-print value="true"/>
        <s:layout-grid-display value="true"/>
      </t:test-style>
      <t:test-bodytext pageruby="1"/>
    </t:testtemplate>
    <t:testtemplate name="layout-grid-ruby-height">
      <t:test-style>
        <s:layout-grid-ruby-height value="25pt"/>
        <s:layout-grid-color value="#c0c0c0"/>
        <s:layout-grid-lines value="15"/>
        <s:layout-grid-mode value="both"/>
        <s:layout-grid-ruby-below value="false"/>
        <s:layout-grid-print value="true"/>
        <s:layout-grid-display value="true"/>
      </t:test-style>
      <t:test-bodytext pageruby="1"/>
    </t:testtemplate>
    <t:testtemplate name="layout-grid-snap-to">
      <t:test-style>
        <s:layout-grid-snap-to value="true"/>
        <s:layout-grid-mode value="both"/>
        <s:layout-grid-standard-mode value="true"/>
        <s:layout-grid-color value="#c0c0c0"/>
        <s:layout-grid-lines value="15"/>
        <s:layout-grid-ruby-below value="false"/>
        <s:layout-grid-print value="true"/>
        <s:layout-grid-display value="true"/>
      </t:test-style>
      <t:test-bodytext>ソビエト連邦のルナ計画により、1959年1月4日に打ち上げられたルナ1号（E-1）は、月の近隣を通過した史上初の月探査機となった。その後継機であるルナ2号は月面に達した。1959年10月7日には、ルナ3号が月の裏側の写真撮影に成功した。その後1966年2月3日に連邦の打ち上げたルナ9号が初の月面「軟着陸」を成し遂げると、1966年4月3日にはルナ10号が月の衛星となることに成功したのであった。</t:test-bodytext>
    </t:testtemplate>
    <!-- default page layout only
		<t:testtemplate name="layout-grid-standard-mode-true">
			<t:test-style>
			<s:layout-grid-standard-mode value="true" />
			<s:layout-grid-mode value="both" />

				    <s:layout-grid-color value="#c0c0c0" />
				    <s:layout-grid-lines value="15" />
				    <s:layout-grid-ruby-below value="false" />
				    <s:layout-grid-print value="true" />
				    <s:layout-grid-display value="true" />

			</t:test-style>
			<t:test-bodytext>ソビエト連邦のルナ計画により、1959年1月4日に打ち上げられたルナ1号（E-1）は、月の近隣を通過した史上初の月探査機となった。その後継機であるルナ2号は月面に達した。1959年10月7日には、ルナ3号が月の裏側の写真撮影に成功した。その後1966年2月3日に連邦の打ち上げたルナ9号が初の月面「軟着陸」を成し遂げると、1966年4月3日にはルナ10号が月の衛星となることに成功したのであった。</t:test-bodytext>
		</t:testtemplate>
		<t:testtemplate name="layout-grid-standard-mode-false">
			<t:test-style>
			<s:layout-grid-standard-mode value="true" />
			<s:layout-grid-mode value="both" />

				    <s:layout-grid-color value="#c0c0c0" />
				    <s:layout-grid-lines value="15" />
				    <s:layout-grid-ruby-below value="false" />
				    <s:layout-grid-print value="true" />
				    <s:layout-grid-display value="true" />

			</t:test-style>
			<t:test-bodytext>ソビエト連邦のルナ計画により、1959年1月4日に打ち上げられたルナ1号（E-1）は、月の近隣を通過した史上初の月探査機となった。その後継機であるルナ2号は月面に達した。1959年10月7日には、ルナ3号が月の裏側の写真撮影に成功した。その後1966年2月3日に連邦の打ち上げたルナ9号が初の月面「軟着陸」を成し遂げると、1966年4月3日にはルナ10号が月の衛星となることに成功したのであった。</t:test-bodytext>
		</t:testtemplate>
-->
    <t:testtemplate name="num-format">
      <t:test-style>
        <s:num-format value="1"/>
      </t:test-style>
      <t:test-bodytext fieldspage="1"/>
    </t:testtemplate>
    <t:testtemplate name="num-format-a">
      <t:test-style>
        <s:num-format value="a"/>
      </t:test-style>
      <t:test-bodytext fieldspage="1"/>
    </t:testtemplate>
    <t:testtemplate name="num-format-i">
      <t:test-style>
        <s:num-format value="i"/>
      </t:test-style>
      <t:test-bodytext fieldspage="1"/>
    </t:testtemplate>
    <t:testtemplate name="num-letter-sync-false">
      <t:test-style>
        <s:num-letter-sync value="false"/>
        <s:num-format value="a"/>
      </t:test-style>
      <t:test-bodytext pagenumberedlist="1"/>
    </t:testtemplate>
    <t:testtemplate name="num-letter-sync-true">
      <t:test-style>
        <s:num-letter-sync value="true"/>
        <s:num-format value="a"/>
      </t:test-style>
      <t:test-bodytext pagenumberedlist="1"/>
    </t:testtemplate>
    <t:testtemplate name="num-prefix">
      <t:test-style>
        <s:num-prefix value="OPEN"/>
      </t:test-style>
      <t:test-bodytext fieldspage="1"/>
    </t:testtemplate>
    <t:testtemplate name="num-suffix">
      <t:test-style>
        <s:num-suffix value="CLOSE"/>
      </t:test-style>
      <t:test-bodytext fieldspage="1"/>
    </t:testtemplate>
    <!--
 		<t:testtemplate name="paper-tray-name">
			<t:test-style>
			<s:paper-tray-name value="memory hole" />
			</t:test-style>
		</t:testtemplate>
-->
    <!-- looking at odf 1.2 spec this is spreadsheet only
 		<t:testtemplate name="print">
			<t:test-style>
			<s:print value="" />
			</t:test-style>
		</t:testtemplate>
-->
    <t:testtemplate name="print-orientation-portrait">
      <t:test-style>
        <s:print-orientation value="portrait"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="print-orientation-landscape">
      <t:test-style>
        <s:print-orientation value="landscape"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="scale-to">
      <t:test-style>
        <s:scale-to value="50%"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="scale-to-pages">
      <t:test-style>
        <s:scale-to-pages value="3"/>
      </t:test-style>
      <t:test-bodytext threepages="1"/>
    </t:testtemplate>
    <t:testtemplate name="shadow">
      <t:test-style>
        <s:shadow value="#808080 -0.0701in 0.0701in"/>
      </t:test-style>
    </t:testtemplate>
    <!--
		<t:testtemplate name="table-centering-h">
			<t:test-style>
			   <s:table-centering value="horizontal" />
			</t:test-style>
			<t:test-bodytext pagetable="1" />
		</t:testtemplate>
		<t:testtemplate name="table-centering-v">
			<t:test-style>
			   <s:table-centering value="vertical" />
			</t:test-style>
			<t:test-bodytext pagetable="1" />
		</t:testtemplate>
		<t:testtemplate name="table-centering-b">
			<t:test-style>
			   <s:table-centering value="both" />
			</t:test-style>
			<t:test-bodytext pagetable="1" />
		</t:testtemplate>
-->
    <t:testtemplate name="writing-mode-rltb">
      <t:test-style>
        <s:writing-mode value="rl-tb"/>
      </t:test-style>
      <t:test-bodytext>بين خبراء من كافة قطاعات الصناعة على الشبكة العالمية انترنيت ويونيكود، حيث ستت </t:test-bodytext>
    </t:testtemplate>
    <t:testtemplate name="writing-mode-tblr">
      <t:test-style>
        <s:writing-mode value="tb-lr"/>
      </t:test-style>
      <t:test-bodytext>開通後の旧坂は県道</t:test-bodytext>
    </t:testtemplate>
  </t:testtemplates>
  <xsl:output encoding="utf-8" indent="no" method="xml" omit-xml-declaration="no"/>
  <xsl:template match="t:testtemplate">
    <xsl:variable name="family">
      <xsl:choose>
        <xsl:when test="$mode='ods'">
          <xsl:value-of select="'table-cell'"/>
        </xsl:when>
        <xsl:when test="$mode='odp'">
          <xsl:value-of select="'graphic'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'paragraph'"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <test name="{$mode}-{@name}">
      <input type="{$mode}1.2">
        <o:styles>
          <s:style s:family="paragraph" s:name="standard">
            <s:text-properties s:font-name="Helvetica" fo:font-size="12pt"/>
          </s:style>
          <s:style s:family="graphic" s:name="standard">
            <s:graphic-properties draw:fill="none" draw:stroke="none"/>
            <s:text-properties s:font-name="Helvetica" fo:font-size="12pt"/>
          </s:style>
          <s:style s:family="table-cell" s:name="standard">
            <s:text-properties s:font-name="Helvetica" fo:font-size="12pt"/>
          </s:style>
          <s:style s:display-name="Text" s:family="text" s:name="color">
            <s:text-properties fo:color="#339999"/>
          </s:style>
          <s:style s:display-name="paratest-style" s:family="{$family}" s:name="parastyle" s:parent-style-name="standard">
            <s:paragraph-properties>
						</s:paragraph-properties>
          </s:style>
          <s:style s:family="table" s:master-page-name="Standard" s:name="table">
            <s:table-properties s:writing-mode="lr-tb" table:display="true"/>
          </s:style>
          <s:style s:family="paragraph" s:name="plpstyle">
            <s:text-properties fo:color="#339999"/>
          </s:style>
          <xsl:choose>
            <xsl:when test="t:test-bodytext[@pagenumberedlist]">
              <text:list-style s:name="L1">
                <text:list-level-style-number s:num-format="a" s:num-suffix=")" text:level="1">
                  <s:list-level-properties text:list-level-position-and-space-mode="label-alignment">
                    <s:list-level-label-alignment text:label-followed-by="listtab" text:list-tab-stop-position="1.27cm" fo:margin-left="1.27cm" fo:text-indent="-0.635cm"/>
                  </s:list-level-properties>
                </text:list-level-style-number>
                <text:list-level-style-number s:num-format="1" s:num-suffix="." text:level="2" text:style-name="Numbering_20_Symbols">
                  <s:list-level-properties text:list-level-position-and-space-mode="label-alignment">
                    <s:list-level-label-alignment text:label-followed-by="listtab" text:list-tab-stop-position="1.905cm" fo:margin-left="1.905cm" fo:text-indent="-0.635cm"/>
                  </s:list-level-properties>
                </text:list-level-style-number>
                <text:list-level-style-number s:num-format="1" s:num-suffix="." text:level="3" text:style-name="Numbering_20_Symbols">
                  <s:list-level-properties text:list-level-position-and-space-mode="label-alignment">
                    <s:list-level-label-alignment text:label-followed-by="listtab" text:list-tab-stop-position="2.54cm" fo:margin-left="2.54cm" fo:text-indent="-0.635cm"/>
                  </s:list-level-properties>
                </text:list-level-style-number>
                <text:list-level-style-number s:num-format="1" s:num-suffix="." text:level="4" text:style-name="Numbering_20_Symbols">
                  <s:list-level-properties text:list-level-position-and-space-mode="label-alignment">
                    <s:list-level-label-alignment text:label-followed-by="listtab" text:list-tab-stop-position="3.175cm" fo:margin-left="3.175cm" fo:text-indent="-0.635cm"/>
                  </s:list-level-properties>
                </text:list-level-style-number>
                <text:list-level-style-number s:num-format="1" s:num-suffix="." text:level="5" text:style-name="Numbering_20_Symbols">
                  <s:list-level-properties text:list-level-position-and-space-mode="label-alignment">
                    <s:list-level-label-alignment text:label-followed-by="listtab" text:list-tab-stop-position="3.81cm" fo:margin-left="3.81cm" fo:text-indent="-0.635cm"/>
                  </s:list-level-properties>
                </text:list-level-style-number>
                <text:list-level-style-number s:num-format="1" s:num-suffix="." text:level="6" text:style-name="Numbering_20_Symbols">
                  <s:list-level-properties text:list-level-position-and-space-mode="label-alignment">
                    <s:list-level-label-alignment text:label-followed-by="listtab" text:list-tab-stop-position="4.445cm" fo:margin-left="4.445cm" fo:text-indent="-0.635cm"/>
                  </s:list-level-properties>
                </text:list-level-style-number>
                <text:list-level-style-number s:num-format="1" s:num-suffix="." text:level="7" text:style-name="Numbering_20_Symbols">
                  <s:list-level-properties text:list-level-position-and-space-mode="label-alignment">
                    <s:list-level-label-alignment text:label-followed-by="listtab" text:list-tab-stop-position="5.08cm" fo:margin-left="5.08cm" fo:text-indent="-0.635cm"/>
                  </s:list-level-properties>
                </text:list-level-style-number>
                <text:list-level-style-number s:num-format="1" s:num-suffix="." text:level="8" text:style-name="Numbering_20_Symbols">
                  <s:list-level-properties text:list-level-position-and-space-mode="label-alignment">
                    <s:list-level-label-alignment text:label-followed-by="listtab" text:list-tab-stop-position="5.715cm" fo:margin-left="5.715cm" fo:text-indent="-0.635cm"/>
                  </s:list-level-properties>
                </text:list-level-style-number>
                <text:list-level-style-number s:num-format="1" s:num-suffix="." text:level="9" text:style-name="Numbering_20_Symbols">
                  <s:list-level-properties text:list-level-position-and-space-mode="label-alignment">
                    <s:list-level-label-alignment text:label-followed-by="listtab" text:list-tab-stop-position="6.35cm" fo:margin-left="6.35cm" fo:text-indent="-0.635cm"/>
                  </s:list-level-properties>
                </text:list-level-style-number>
                <text:list-level-style-number s:num-format="1" s:num-suffix="." text:level="10" text:style-name="Numbering_20_Symbols">
                  <s:list-level-properties text:list-level-position-and-space-mode="label-alignment">
                    <s:list-level-label-alignment text:label-followed-by="listtab" text:list-tab-stop-position="6.985cm" fo:margin-left="6.985cm" fo:text-indent="-0.635cm"/>
                  </s:list-level-properties>
                </text:list-level-style-number>
              </text:list-style>
            </xsl:when>
          </xsl:choose>
        </o:styles>
        <o:automatic-styles>
          <s:page-layout s:name="TestStyle">
            <s:page-layout-properties>
              <xsl:for-each select="t:test-style/*">
                <xsl:attribute name="{name()}" namespace="{namespace-uri()}">
                  <xsl:value-of select="@value"/>
                </xsl:attribute>
              </xsl:for-each>
            </s:page-layout-properties>
          </s:page-layout>
          <s:style s:family="table" s:master-page-name="Standard" s:name="table">
            <s:table-properties s:writing-mode="lr-tb" table:display="true"/>
          </s:style>
        </o:automatic-styles>
        <o:master-styles>
          <s:master-page s:name="Standard" s:page-layout-name="TestStyle"/>
        </o:master-styles>
        <xsl:choose>
          <xsl:when test="t:test-bodytext[@pageruby]">
            <xsl:call-template name="body">
              <xsl:with-param name="pageruby" select="1"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="t:test-bodytext[@pagenumberedlist]">
            <xsl:call-template name="body">
              <xsl:with-param name="pagenumberedlist" select="1"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="t:test-bodytext[@pagetable]">
            <xsl:call-template name="body">
              <xsl:with-param name="pagetable" select="1"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="t:test-bodytext[@pagefootnote]">
            <xsl:call-template name="body">
              <xsl:with-param name="pagefootnote" select="1"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="t:test-bodytext[@fieldspage]">
            <xsl:call-template name="body">
              <xsl:with-param name="fieldspage" select="1"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="t:test-bodytext[@ipsum]">
            <xsl:call-template name="body">
              <xsl:with-param name="bodytext" select="$Loremipsum"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="t:test-bodytext[@threepages]">
            <xsl:call-template name="body">
              <xsl:with-param name="threepages" select="1"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="t:test-bodytext[@threeshorttext]">
            <xsl:call-template name="body">
              <xsl:with-param name="threeshorttext" select="1"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="t:test-bodytext[@pageswithstyle]">
            <xsl:call-template name="body">
              <xsl:with-param name="pageswithstyle" select="1"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="string-length(t:test-bodytext) != 0">
            <xsl:call-template name="body">
              <xsl:with-param name="bodytext" select="concat('',t:test-bodytext)"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name="body">
				</xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </input>
      <output types="{$mode}1.0 {$mode}1.1 {$mode}1.2 {$mode}1.2ext">
        <file path="styles.xml">
          <xsl:for-each select="t:test-style/*">
            <xsl:variable name="selector" select="concat(&quot;//s:page-layout[1]/s:page-layout-properties/@&quot;,name())"/>
            <xpath expr="boolean({$selector})"/>
            <xsl:call-template name="xpaths">
              <xsl:with-param name="selector" select="$selector"/>
              <xsl:with-param name="value" select="@value"/>
              <xsl:with-param name="index" select="-1"/>
            </xsl:call-template>
          </xsl:for-each>
        </file>
      </output>
      <pdf/>
    </test>
  </xsl:template>
  <xsl:template match="/">
    <documenttests xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.example.org/documenttests ../documenttests.xsd">
      <xsl:variable name="tests" select="/xsl:stylesheet/t:testtemplates/*"/>
      <xsl:apply-templates select="$tests"/>
    </documenttests>
  </xsl:template>
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
