<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns="http://www.example.org/documenttests" xmlns:anim="urn:oasis:names:tc:opendocument:xmlns:animation:1.0" xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0" xmlns:config="urn:oasis:names:tc:opendocument:xmlns:config:1.0" xmlns:db="urn:oasis:names:tc:opendocument:xmlns:database:1.0" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0" xmlns:manifest="urn:oasis:names:tc:opendocument:xmlns:manifest:1.0" xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0" xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datas:1.0" xmlns:o="urn:oasis:names:tc:opendocument:xmlns:office:1.0" xmlns:odf="http://docs.oasis-open.org/ns/office/1.2/meta/odf#" xmlns:of="urn:oasis:names:tc:opendocument:xmlns:of:1.2" xmlns:presentation="urn:oasis:names:tc:opendocument:xmlns:presentation:1.0" xmlns:s="urn:oasis:names:tc:opendocument:xmlns:style:1.0" xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0" xmlns:smil="urn:oasis:names:tc:opendocument:xmlns:smil-compatible:1.0" xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0" xmlns:t="http://www.example.org/documenttests" xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:import href="shared.xsl"/>
  <xsl:param name="mode" select="'odt'"/>
  <xsl:param name="test" select="'paragraph'"/>
  <xsl:param name="props" select="'paragraph'"/>
  <t:testtemplates>
    <t:testtemplate name="background-color">
      <t:test-style>
        <fo:background-color value="#339999"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border">
      <t:test-style>
        <fo:border value="2pt solid #000000"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border-bottom">
      <t:test-style>
        <fo:border-bottom value="2pt solid #000000"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border-left">
      <t:test-style>
        <fo:border-left value="2pt solid #000000"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border-right">
      <t:test-style>
        <fo:border-right value="2pt solid #000000"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border-top">
      <t:test-style>
        <fo:border-top value="2pt solid #000000"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="break-after">
      <t:test-style>
        <fo:break-after value="page"/>
      </t:test-style>
      <t:test-bodytext threepages="1"/>
    </t:testtemplate>
    <t:testtemplate name="break-before">
      <t:test-style>
        <fo:break-before value="page"/>
      </t:test-style>
      <t:test-bodytext threepages="1"/>
    </t:testtemplate>
    <t:testtemplate name="hyphenation-keep">
      <t:test-style>
        <fo:hyphenation-keep value="page"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="hyphenation-ladder-count">
      <t:test-style>
        <fo:hyphenation-ladder-count value="no-limit"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="keep-together">
      <t:middle-style>
        <fo:keep-together value="always"/>
      </t:middle-style>
      <t:test-style>
        <fo:keep-together value="auto"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="keep-with-next">
      <t:middle-style>
        <fo:keep-with-next value="always"/>
      </t:middle-style>
      <t:test-style>
        <fo:keep-with-next value="auto"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="line-height">
      <t:test-style>
        <fo:line-height value="200%"/>
      </t:test-style>
      <t:test-bodytext threeshorttext="1"/>
    </t:testtemplate>
    <t:testtemplate name="margin">
      <t:test-style>
        <fo:margin value="2in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="margin-bottom">
      <t:test-style>
        <fo:margin-bottom value="1in"/>
      </t:test-style>
      <t:test-bodytext threeshorttext="1"/>
    </t:testtemplate>
    <t:testtemplate name="margin-left">
      <t:test-style>
        <fo:margin-left value="1in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="margin-right">
      <t:test-style>
        <fo:margin-right value="1in"/>
      </t:test-style>
      <t:test-bodytext threeshorttext="1"/>
    </t:testtemplate>
    <t:testtemplate name="margin-top">
      <t:test-style>
        <fo:margin-top value="1in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="orphans">
      <t:test-style>
        <fo:orphans value="10"/>
      </t:test-style>
      <t:test-bodytext pageswithstyle="1"/>
    </t:testtemplate>
    <t:testtemplate name="padding">
      <t:test-style>
        <fo:padding value="1.5in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="padding-bottom">
      <t:test-style>
        <fo:padding-bottom value="4.5in"/>
      </t:test-style>
      <t:test-bodytext threeshorttext="1"/>
    </t:testtemplate>
    <t:testtemplate name="padding-left">
      <t:test-style>
        <fo:padding-left value="1.5in"/>
      </t:test-style>
      <t:test-bodytext threeshorttext="1"/>
    </t:testtemplate>
    <t:testtemplate name="padding-right">
      <t:test-style>
        <fo:padding-right value="1.5in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="padding-top">
      <t:test-style>
        <fo:padding-top value="1.5in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-align">
      <t:test-style>
        <fo:text-align value="center"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-align-last">
      <t:test-style>
        <fo:text-align value="right"/>
        <fo:text-align-last value="center"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-indent">
      <t:test-style>
        <fo:text-indent value="10mm"/>
        <fo:background-color value="#339999"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-indent-negative">
      <t:test-style>
        <fo:text-indent value="-10mm"/>
        <fo:background-color value="#339999"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="widows">
      <t:test-style>
        <fo:widows value="10"/>
      </t:test-style>
      <t:test-bodytext pageswithstyle="1"/>
    </t:testtemplate>
    <t:testtemplate name="auto-test-indent">
      <t:test-style>
        <s:auto-text-indent value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="background-transparency">
      <t:middle-style>
        <fo:background-color value="#FF0000"/>
      </t:middle-style>
      <t:test-style>
        <s:background-transparency value="90%"/>
        <fo:background-color value="#00FF00"/>
      </t:test-style>
      <t:test-bodytext>This should have a mostly Red background due to transparency.</t:test-bodytext>
    </t:testtemplate>
    <t:testtemplate name="border-line-width">
      <t:test-style>
        <fo:border value="5pt double #000000"/>
        <s:border-line-width value="0.4mm 0.0299in 0.7mm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border-line-width-bottom">
      <t:test-style>
        <fo:border-bottom value="5pt double #000000"/>
        <s:border-line-width-bottom value="0.4mm 0.0299in 0.7mm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border-line-width-left">
      <t:test-style>
        <fo:border-left value="5pt double #000000"/>
        <s:border-line-width-left value="0.4mm 0.0299in 0.7mm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border-line-width-right">
      <t:test-style>
        <fo:border-right value="5pt double #000000"/>
        <s:border-line-width-right value="0.4mm 0.0299in 0.7mm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="border-line-width-top">
      <t:test-style>
        <fo:border-top value="5pt double #000000"/>
        <s:border-line-width-top value="0.4mm 0.0299in 0.7mm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="font-independent-line-spacing">
      <t:test-style>
        <s:font-independent-line-spacing value="false"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="join-border">
      <t:test-style>
        <s:join-border value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="justify-single-word">
      <t:test-style>
        <s:justify-single-word value="true"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="line-break">
      <t:middle-style>
        <s:line-break value="strict"/>
      </t:middle-style>
      <t:test-style>
        <s:line-break value="normal"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="line-height-at-least">
      <t:test-style>
        <s:line-height-at-least value="2in"/>
      </t:test-style>
      <t:test-bodytext threeshorttext="1"/>
    </t:testtemplate>
    <t:testtemplate name="line-spacing">
      <t:test-style>
        <s:line-spacing value="10mm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="page-number">
      <t:test-style>
        <s:page-number value="10"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="punctuation-wrap">
      <t:middle-style>
        <s:punctuation-wrap value="hanging"/>
      </t:middle-style>
      <t:test-style>
        <s:punctuation-wrap value="simple"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="register-true">
      <t:middle-style>
        <s:register-true value="true"/>
      </t:middle-style>
      <t:test-style>
        <s:register-true value="false"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="shadow">
      <t:test-style>
        <s:shadow value="#808080 -0.0701in 0.0701in"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="snap-to-layout-grid">
      <t:middle-style>
        <s:snap-to-layout-grid value="true"/>
      </t:middle-style>
      <t:test-style>
        <s:snap-to-layout-grid value="false"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="tab-stop-distance">
      <t:test-style>
        <s:tab-stop-distance value="50mm"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="text-autospace">
      <t:middle-style>
        <s:text-autospace value="ideograph-alpha"/>
      </t:middle-style>
      <t:test-style>
        <s:text-autospace value="none"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="vertical-align">
      <t:middle-style>
        <s:vertical-align value="bottom"/>
      </t:middle-style>
      <t:test-style>
        <s:vertical-align value="auto"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="writing-mode">
      <t:middle-style>
        <s:writing-mode value="rl-tb"/>
      </t:middle-style>
      <t:test-style>
        <s:writing-mode value="lr-tb"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="writing-mode-automatic">
      <t:middle-style>
        <s:writing-mode-automatic value="true"/>
      </t:middle-style>
      <t:test-style>
        <s:writing-mode-automatic value="false"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="line-number">
      <t:test-style>
        <text:number-lines value="true"/>
        <text:line-number value="5"/>
      </t:test-style>
    </t:testtemplate>
    <t:testtemplate name="number-lines">
      <t:test-style>
        <text:number-lines value="true"/>
      </t:test-style>
    </t:testtemplate>
  </t:testtemplates>
  <xsl:output encoding="utf-8" indent="no" method="xml" omit-xml-declaration="no"/>
  <xsl:template match="t:testtemplate">
    <xsl:variable name="family">
      <xsl:choose>
        <xsl:when test="$mode='ods'">
          <xsl:value-of select="'table-cell'"/>
        </xsl:when>
        <xsl:when test="$mode='odp'">
          <xsl:value-of select="'graphic'"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'paragraph'"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <test name="{$mode}-{@name}">
      <input type="{$mode}1.2">
        <o:styles>
          <s:style s:family="paragraph" s:name="standard">
            <s:text-properties s:font-name="Helvetica" fo:font-size="12pt"/>
          </s:style>
          <s:style s:family="graphic" s:name="standard">
            <s:graphic-properties draw:fill="none" draw:stroke="none"/>
            <s:text-properties s:font-name="Helvetica" fo:font-size="12pt"/>
          </s:style>
          <s:style s:family="table-cell" s:name="standard">
            <s:text-properties s:font-name="Helvetica" fo:font-size="12pt"/>
          </s:style>
          <s:style s:display-name="Text" s:family="text" s:name="color">
            <s:text-properties fo:color="#339999"/>
          </s:style>
          <s:style s:display-name="middle" s:family="{$family}" s:name="middle" s:parent-style-name="standard">
            <s:paragraph-properties>
              <xsl:for-each select="t:middle-style/*">
                <xsl:attribute name="{name()}" namespace="{namespace-uri()}">
                  <xsl:value-of select="@value"/>
                </xsl:attribute>
              </xsl:for-each>
            </s:paragraph-properties>
          </s:style>
          <s:style s:display-name="TestStyle" s:family="{$family}" s:name="style" s:parent-style-name="middle">
            <s:paragraph-properties>
              <xsl:for-each select="t:test-style/*">
                <xsl:attribute name="{name()}" namespace="{namespace-uri()}">
                  <xsl:value-of select="@value"/>
                </xsl:attribute>
              </xsl:for-each>
            </s:paragraph-properties>
          </s:style>
          <s:style s:family="table" s:master-page-name="Standard" s:name="table">
            <s:table-properties s:writing-mode="lr-tb" table:display="true"/>
          </s:style>
        </o:styles>
        <o:automatic-styles>
          <s:style s:family="table" s:master-page-name="Standard" s:name="table">
            <s:table-properties s:writing-mode="lr-tb" table:display="true"/>
          </s:style>
        </o:automatic-styles>
        <xsl:choose>
          <xsl:when test="t:test-bodytext[@ipsum]">
            <xsl:call-template name="body">
              <xsl:with-param name="bodytext" select="$Loremipsum"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="t:test-bodytext[@threepages]">
            <xsl:call-template name="body">
              <xsl:with-param name="threepages" select="1"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="t:test-bodytext[@threeshorttext]">
            <xsl:call-template name="body">
              <xsl:with-param name="threeshorttext" select="1"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="t:test-bodytext[@pageswithstyle]">
            <xsl:call-template name="body">
              <xsl:with-param name="pageswithstyle" select="1"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="string-length(t:test-bodytext) != 0">
            <xsl:call-template name="body">
              <xsl:with-param name="bodytext" select="concat('',t:test-bodytext)"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name="body">
				</xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </input>
      <output types="{$mode}1.0 {$mode}1.1 {$mode}1.2 {$mode}1.2ext">
        <file path="styles.xml">
          <xsl:for-each select="t:test-style/*">
            <xsl:variable name="selector" select="concat(&quot;//s:style[@s:display-name='TestStyle' or (not(@s:display-name) and @s:name='TestStyle')]/s:paragraph-properties/@&quot;,name())"/>
            <xpath expr="boolean({$selector})"/>
            <xsl:call-template name="xpaths">
              <xsl:with-param name="selector" select="$selector"/>
              <xsl:with-param name="value" select="@value"/>
              <xsl:with-param name="index" select="-1"/>
            </xsl:call-template>
          </xsl:for-each>
        </file>
      </output>
      <pdf/>
    </test>
  </xsl:template>
  <xsl:template match="/">
    <documenttests xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.example.org/documenttests ../documenttests.xsd">
      <xsl:variable name="tests" select="/xsl:stylesheet/t:testtemplates/*"/>
      <xsl:apply-templates select="$tests"/>
    </documenttests>
  </xsl:template>
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
