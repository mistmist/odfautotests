# ODFAutoTests

Read the [fine introduction](doc/01_introduction.md) to learn more.

If you are just looking for some [results](http://autotests.opendocumentformat.org), please go to [http://autotests.opendocumentformat.org](http://autotests.opendocumentformat.org)

## Requirements

For use:
- Java Runtime Environment

For development:
- Java SDK
- Ant build system